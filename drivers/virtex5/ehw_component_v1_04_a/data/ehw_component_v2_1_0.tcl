##############################################################################
## Filename:          /home/micro/hera_v5_srlut/drivers/ehw_component_v1_04_a/data/ehw_component_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Wed Feb  1 11:49:51 2012 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "ehw_component" "NUM_INSTANCES" "DEVICE_ID" "C_BASEADDR" "C_HIGHADDR" "C_MEM0_BASEADDR" "C_MEM0_HIGHADDR" 
}

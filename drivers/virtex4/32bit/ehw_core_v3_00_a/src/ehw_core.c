//////////////////////////////////////////////////////////////////////////////
// Filename:          C:\Tesi\ProvaEDK2/drivers/ehw_core_v2_00_a/src/ehw_core.c
// Version:           2.00.a
// Description:       ehw_core Driver Source File
// Date:              Thu Mar 12 14:44:16 2009 (by Create and Import Peripheral Wizard)
//////////////////////////////////////////////////////////////////////////////


#include "ehw_core.h"
#include "xparameters.h"

#define RESET_BIT		(0x2000000)
#define START_BIT 	(0x1000000)
#define ACK_BIT_MASK	(0x1000000)
#define REPS			2

/************************** Function Definitions ***************************/

void printBit(Xuint32 dato)
{
	Xuint32 probe=1;
	Xuint8 i,j;
	
	for(j=0; j<4; j++)
	{
		xil_printf("bit[%d-%d]: ",j*8,(j+1)*8-1);
		for(i=0; i<8; i++, probe <<= 1)
		{
			if(dato & probe)
				xil_printf("1");
			else
				xil_printf("0");
		}
		xil_printf("\n\r");
	}
	
	
}

int main() {
	
	Xuint32 data;
	Xuint16 i;
	
	
	
	xil_printf("Scrittura Reset Bit...");
	EHW_CORE_mWriteCtrlReg(XPAR_EHW_CORE_0_BASEADDR,0,RESET_BIT);
	
	xil_printf("OK\n\rAttendo conferma...");
	do {
		data = EHW_CORE_mReadStatusReg(XPAR_EHW_CORE_0_BASEADDR, 0);
		//if (data == (Xuint32) 0)
			xil_printf("data: [0x%X]\n\r",data);
		//else
		//	xil_printf("Not Zero\n\r");
	} while( (data & ACK_BIT_MASK) == 0 );
	xil_printf("OK!\n\r");
	
	while(1)
	{
	//for(i=0; i<REPS; i++)
	//{
		
		xil_printf("Reset Ctrl Reg...");
		EHW_CORE_mWriteCtrlReg(XPAR_EHW_CORE_0_BASEADDR,0,0);
		xil_printf("OK\n\r");
	
		xil_printf("Input data: ");
		iscanf("%d",&data);
		data<<=24; 
		xil_printf("Letto: %X\n\r",data);
		
		printBit(data);
		
		xil_printf("Scrittura Dati Input...");
		EHW_CORE_mWriteInputReg(XPAR_EHW_CORE_0_BASEADDR,0,data);
		xil_printf("OK\n\r");
	
		xil_printf("Scrittura Start Bit...");
		EHW_CORE_mWriteCtrlReg(XPAR_EHW_CORE_0_BASEADDR,0,START_BIT);
	
		xil_printf("OK\n\rAttendo conferma...");
		do {
			data = EHW_CORE_mReadStatusReg(XPAR_EHW_CORE_0_BASEADDR, 0);
		} while( (data & ACK_BIT_MASK) == 0 );
		xil_printf("OK!\n\r");
	
		data = EHW_CORE_mReadOutputReg(XPAR_EHW_CORE_0_BASEADDR, 0);
	
		xil_printf("Dato Letto: %X\n\r", data);
		printBit(data);
	}
	return 0;
}

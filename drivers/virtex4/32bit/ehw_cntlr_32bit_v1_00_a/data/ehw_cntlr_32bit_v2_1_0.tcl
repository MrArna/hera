##############################################################################
## Filename:          /home/micro/hera8bit_150MHz/drivers/ehw_cntlr_32bit_v1_00_a/data/ehw_cntlr_32bit_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Fri Apr  8 10:35:34 2011 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "ehw_cntlr_32bit" "NUM_INSTANCES" "DEVICE_ID" "C_BASEADDR" "C_HIGHADDR" "C_MEM0_BASEADDR" "C_MEM0_HIGHADDR" "C_MEM1_BASEADDR" "C_MEM1_HIGHADDR" 
}

/*****************************************************************************
* Filename:          /home/micro/hera8bit_150MHz/drivers/ehw_cntlr_32bit_v1_00_a/src/ehw_cntlr_32bit.h
* Version:           1.00.a
* Description:       ehw_cntlr_32bit Driver Header File
* Date:              Fri Apr  8 10:35:34 2011 (by Create and Import Peripheral Wizard)
*****************************************************************************/

#ifndef EHW_CNTLR_32BIT_H
#define EHW_CNTLR_32BIT_H

/***************************** Include Files *******************************/

#include "xbasic_types.h"
#include "xstatus.h"
#include "xil_io.h"

/************************** Constant Definitions ***************************/


/**
 * User Logic Slave Space Offsets
 * -- SLV_REG0 : user logic slave module register 0
 * -- SLV_REG1 : user logic slave module register 1
 * -- SLV_REG2 : user logic slave module register 2
 * -- SLV_REG3 : user logic slave module register 3
 */
#define EHW_CNTLR_32BIT_USER_SLV_SPACE_OFFSET (0x00000000)
#define EHW_CNTLR_32BIT_SLV_REG0_OFFSET (EHW_CNTLR_32BIT_USER_SLV_SPACE_OFFSET + 0x00000000)
#define EHW_CNTLR_32BIT_SLV_REG1_OFFSET (EHW_CNTLR_32BIT_USER_SLV_SPACE_OFFSET + 0x00000004)
#define EHW_CNTLR_32BIT_SLV_REG2_OFFSET (EHW_CNTLR_32BIT_USER_SLV_SPACE_OFFSET + 0x00000008)
#define EHW_CNTLR_32BIT_SLV_REG3_OFFSET (EHW_CNTLR_32BIT_USER_SLV_SPACE_OFFSET + 0x0000000C)

/**************************** Type Definitions *****************************/


/***************** Macros (Inline Functions) Definitions *******************/

/**
 *
 * Write a value to a EHW_CNTLR_32BIT register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the EHW_CNTLR_32BIT device.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void EHW_CNTLR_32BIT_mWriteReg(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Data)
 *
 */
#define EHW_CNTLR_32BIT_mWriteReg(BaseAddress, RegOffset, Data) \
 	Xil_Out32((BaseAddress) + (RegOffset), (Xuint32)(Data))

/**
 *
 * Read a value from a EHW_CNTLR_32BIT register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the EHW_CNTLR_32BIT device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	Xuint32 EHW_CNTLR_32BIT_mReadReg(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define EHW_CNTLR_32BIT_mReadReg(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (RegOffset))


/**
 *
 * Write/Read 32 bit value to/from EHW_CNTLR_32BIT user logic slave registers.
 *
 * @param   BaseAddress is the base address of the EHW_CNTLR_32BIT device.
 * @param   RegOffset is the offset from the slave register to write to or read from.
 * @param   Value is the data written to the register.
 *
 * @return  Data is the data from the user logic slave register.
 *
 * @note
 * C-style signature:
 * 	void EHW_CNTLR_32BIT_mWriteSlaveRegn(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Value)
 * 	Xuint32 EHW_CNTLR_32BIT_mReadSlaveRegn(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define EHW_CNTLR_32BIT_mWriteSlaveReg0(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (EHW_CNTLR_32BIT_SLV_REG0_OFFSET) + (RegOffset), (Xuint32)(Value))
#define EHW_CNTLR_32BIT_mWriteSlaveReg1(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (EHW_CNTLR_32BIT_SLV_REG1_OFFSET) + (RegOffset), (Xuint32)(Value))
#define EHW_CNTLR_32BIT_mWriteSlaveReg2(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (EHW_CNTLR_32BIT_SLV_REG2_OFFSET) + (RegOffset), (Xuint32)(Value))
#define EHW_CNTLR_32BIT_mWriteSlaveReg3(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (EHW_CNTLR_32BIT_SLV_REG3_OFFSET) + (RegOffset), (Xuint32)(Value))

#define EHW_CNTLR_32BIT_mReadSlaveReg0(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (EHW_CNTLR_32BIT_SLV_REG0_OFFSET) + (RegOffset))
#define EHW_CNTLR_32BIT_mReadSlaveReg1(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (EHW_CNTLR_32BIT_SLV_REG1_OFFSET) + (RegOffset))
#define EHW_CNTLR_32BIT_mReadSlaveReg2(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (EHW_CNTLR_32BIT_SLV_REG2_OFFSET) + (RegOffset))
#define EHW_CNTLR_32BIT_mReadSlaveReg3(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (EHW_CNTLR_32BIT_SLV_REG3_OFFSET) + (RegOffset))

/**
 *
 * Write/Read 32 bit value to/from EHW_CNTLR_32BIT user logic memory (BRAM).
 *
 * @param   Address is the memory address of the EHW_CNTLR_32BIT device.
 * @param   Data is the value written to user logic memory.
 *
 * @return  The data from the user logic memory.
 *
 * @note
 * C-style signature:
 * 	void EHW_CNTLR_32BIT_mWriteMemory(Xuint32 Address, Xuint32 Data)
 * 	Xuint32 EHW_CNTLR_32BIT_mReadMemory(Xuint32 Address)
 *
 */
#define EHW_CNTLR_32BIT_mWriteMemory(Address, Data) \
 	Xil_Out32(Address, (Xuint32)(Data))
#define EHW_CNTLR_32BIT_mReadMemory(Address) \
 	Xil_In32(Address)

/************************** Function Prototypes ****************************/


/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the EHW_CNTLR_32BIT instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus EHW_CNTLR_32BIT_SelfTest(void * baseaddr_p);

#endif /** EHW_CNTLR_32BIT_H */

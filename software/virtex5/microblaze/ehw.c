/*
 * @title Hera Project
 * @part Interaction with the EHW controller, fitness functions
 * @author Davide Bartolini
 * @author Fabio Cancare
 * @author Matteo Carminati
 */
#include "ehw.h"

void memory_test() {
	int32 i;
	printf("HERA-V5 SRLUT Testing\n\r\n\r\n\r");
	printf("Testing of the 32bit EHW controller 1st memory\n\r");
	for(i=0; i < LUT_MEMORY_SIZE; i++) {
		EHW_COMPONENT_mWriteMemory(i*4 + XPAR_EHW_COMPONENT_0_MEM0_BASEADDR, i);
	}
	for(i=0; i < LUT_MEMORY_SIZE; i++) {
		if(i != EHW_COMPONENT_mReadMemory(i*4 + XPAR_EHW_COMPONENT_0_MEM0_BASEADDR)) {
			printf("Wrong 32bit EHW controller memory content, %d\n\r", i);
			exit(1);
		}
	}
	printf("Memory test DONE\n\r");
}

void lut_test() {
	unsigned int ehw_in0, ehw_out0 = 0, k;
	/*
	printf("Start random TEST\n\r");
	for(k=0;k<100;k++) {
		ehw_in0 = rand()%INT_MAX;
		ehwWrite(ehw_in0);
		ehwRead(&ehw_out0);
		printf("trial %d.     IN: %X OUT: %X\n\r", k, ehw_in0, ehw_out0);
	}*/

	printf("Reconfiguration, all LUTs set to 0xFFFFFFFF\n\r");
	deployToFPGA(NULL, 0xFFFFFFFF);
	printf("DONE\n\r");

	printf("Start random TEST\n\r");
	for(k=0;k<5;k++) {
		ehw_in0 = rand()%INT_MAX;
		ehwWrite(ehw_in0);
			ehwRead(&ehw_out0);
			printf("trial %d.     IN: %X OUT: %X\n\r", k, ehw_in0, ehw_out0);
	}

	printf("Reconfiguration, all LUTs set to 0\n\r");
	deployToFPGA(NULL, 0);
	printf("DONE\n\r");

	printf("Start random TEST\n\r");
	for(k=0;k<5;k++) {
		ehw_in0 = rand()%INT_MAX;
		ehwWrite(ehw_in0);
			ehwRead(&ehw_out0);
			printf("trial %d.     IN: %X OUT: %X\n\r", k, ehw_in0, ehw_out0);
	}
}

/*
 * Evaluate the stopping criteria
 * @returns 1 if the stopping criteria are met, 0 otherwise
 */
int resultFound(Population *pop, int fitnessMax) {
 	int i, j, k, l;
 	int found = 0;
 	if(!END_ON_MAXIMUM_FITNESS) {
 		return found;
 	}
 	else {
 		found = 1;
 		for(i = 0; i < CONVERGED_SOLUTIONS_MIN_NUMBER && found; i++) {
 			if(pop->candidates[i]->fitness < fitnessMax) {
 				found = 0;
 			}
 		}
 		if(found) {
 			printf("###@@@!!! Goal met at generation %d !!!@@@###\n\r", pop->generation_number);
 			if(DEBUG_LEVEL >= 2) {
				for(i = 0; i < CONVERGED_SOLUTIONS_MIN_NUMBER; i++) {
					printf("Best individual %d, fitness: %d (LUTs content):\n\r", i + 1, pop->candidates[i]->fitness);
					for(j = 0; j < PARTS_PER_CANDIDATE_SOLUTION; j++) {
						for(k = 0; k < MODULES_PER_PART; k++) {
							for(l = 0; l < CELLS_PER_MODULE; l++) {
								printf("0x%X", pop->candidates[i]->parts[j].modules[k].cells[l].luts);
								if(k < MODULES_PER_PART - 1 || l < CELLS_PER_MODULE - 1) {
									printf(", ");
								}
							}
						}
					}
				}
				printf("\n\r\n\r");
 			}
 		}
 		return found;
 	}
 }

int chambolle() {
	//FIXME
	return 0;
}

int adder(unsigned int fitnessMax) {
	int32 i, j, k, input, output, desired_output, fitness = 0;
	const int32 mask = 0x00000010;
	for(i = 0; i < 16; i++) {
		for(j = 0; j < 16; j++) {
			input = i*mask + j;
			desired_output = i + j;
			ehwWrite(input);
			ehwRead(&output);
			for(k = 0; k < 4; k++) {
				if((output & k) == (desired_output & k)) {
					fitness++;
				}
			}
		}
	}
	return fitness;
}

/*
 * Fitness function for evolve a parity generator
 * @returns int fitness: the candidate solution fitness
 */
int parity_generator(unsigned int fitnessMax) {
	int32 j, output;
	int32 fitness = 0;
	volatile int32 v;
	int32 desired_output;
	const int32 mask = 0x00000001;
	for(j = 0; j < fitnessMax; j++) {
		ehwWrite(j);
		ehwRead(&output);
		//Software parity computation
		v = j;
		v ^= v >> 1;
		v ^= v >> 2;
		v = (v & 0x11111111U) * 0x11111111U;
		desired_output = (v >> 28) & 1;
		if((mask & output) == desired_output) {
			fitness++;
		}
	}
	return fitness;
}

/*
 * Write data to the candidate solution input register
 * @param int32 data: input data
 */
void ehwWrite(volatile int32 data0) {
	//Polling for ehw_component to be read
	if(DEBUG_LEVEL >= 3) {
		printf("Value of the status register: %X\n\r", (unsigned int)EHW_COMPONENT_mReadSlaveReg1(XPAR_EHW_COMPONENT_0_BASEADDR, 0));
		printf("Waiting for 0xFFFFFFFF\n\r");
	}
	while(EHW_COMPONENT_mReadSlaveReg1(XPAR_EHW_COMPONENT_0_BASEADDR, 0) != 0xFFFFFFFF);
	//Write the data
	EHW_COMPONENT_mWriteSlaveReg2(XPAR_EHW_COMPONENT_0_BASEADDR, 0, data0);
	//Start computation
	EHW_COMPONENT_mWriteSlaveReg0(XPAR_EHW_COMPONENT_0_BASEADDR, 0, 0x00000001);
}

/*
 * //Read the candidate solution output register
 * @returns: int32: output data
 */
void ehwRead(int32 *data0) {
 	//Wait for the end of the computation
	if(DEBUG_LEVEL >= 3) {
		printf("Value of the status register: %X\n\r", (unsigned int)EHW_COMPONENT_mReadSlaveReg1(XPAR_EHW_COMPONENT_0_BASEADDR, 0));
		printf("Waiting for 1\n\r");
	}
	while(EHW_COMPONENT_mReadSlaveReg1(XPAR_EHW_COMPONENT_0_BASEADDR, 0) != 1);
 	//Return the content of the output register
	*data0 = (int32)EHW_COMPONENT_mReadSlaveReg3(XPAR_EHW_COMPONENT_0_BASEADDR, 0);
	//End computation
	EHW_COMPONENT_mWriteSlaveReg0(XPAR_EHW_COMPONENT_0_BASEADDR, 0, 0);
}

int deployToFPGA(Candidate *candidate, unsigned int datum) {
	int32 i, address = 0;
	int32 part, module, cell;

	//Polling for ehw_component to be ready
	if(DEBUG_LEVEL >= 3) {
		printf("Value of the status register: %X\n\r", (unsigned int)EHW_COMPONENT_mReadSlaveReg1(XPAR_EHW_COMPONENT_0_BASEADDR, 0));
		printf("Waiting for a value different than 0\n\r");
	}
	while(!EHW_COMPONENT_mReadSlaveReg1(XPAR_EHW_COMPONENT_0_BASEADDR, 0));
	//Write the reconfiguration data
	if(candidate == NULL) {
		printf("Candidate is NULL, setting all LUTs to %X\n\r", datum);
		for(i=0; i < LUT_MEMORY_SIZE; i++) {
			EHW_COMPONENT_mWriteMemory(i*4 + XPAR_EHW_COMPONENT_0_MEM0_BASEADDR, datum);
		}
	}
	else {
		part = 0;
		address = 0;
		for(i=0; i < 16; i++) {
			for(module = 0; module < MODULES_PER_PART; module++) {
				for(cell = 0; cell < CELLS_PER_MODULE; cell++) {
					EHW_COMPONENT_mWriteMemory(address*4 + XPAR_EHW_COMPONENT_0_MEM0_BASEADDR, candidate->parts[part].modules[module].cells[cell].luts);
					address++;
				}
			}
			part++;
		}
	}
	//Start the reconfiguration
	if(DEBUG_LEVEL >= 3) printf("Starting the reconfiguration\n\r");
	EHW_COMPONENT_mWriteSlaveReg0(XPAR_EHW_COMPONENT_0_BASEADDR, 0, 0xFFFFFFFE);
	//Wait for the reconfiguration end
	if(DEBUG_LEVEL >= 3) printf("Waiting the end of the reconfiguration process\n\r");
	while(EHW_COMPONENT_mReadSlaveReg1(XPAR_EHW_COMPONENT_0_BASEADDR, 0) != 0x00000002);
	if(DEBUG_LEVEL >= 3) printf("Resetting the ehw_component\n\r");
	EHW_COMPONENT_mWriteSlaveReg0(XPAR_EHW_COMPONENT_0_BASEADDR, 0, 0);
	if(DEBUG_LEVEL >= 3) printf("Deploy to FPGA done\n\r");
	return 0;
}

/*
 * Function for assigning a fitness value to a candidate solution in a population
 * @param int *fitness: where to write the fitness value
 * @param int fitnessFunction: which fitness function must be used
 * @param unsigned int fitnessMax: maximum value for the fitness
 */
void fitness(int* fitness, int fitnessFunction, unsigned int fitnessMax, int verbose) {
	switch(fitnessFunction) {
		case 0:
			*fitness = chambolle();
		break;

		case 1:
			*fitness = parity_generator(fitnessMax);
		break;

		case 2:
			*fitness = adder(fitnessMax);
		break;

		default:
			printf("The fitness function specified does not exist!\n\r");
			exit(1);
		break;

	}
	if(DEBUG_LEVEL >= 2) {
		printf("Computed fitness: %d\n\r", *fitness);
	}
	return;
}

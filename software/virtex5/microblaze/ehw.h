 /*
 * @title Hera Project
 * @author Davide Bartolini
 * @author Fabio Cancare
 * @author Matteo Carminati
 */

#ifndef EHW_H_
#define EHW_H_

#include <xparameters.h>
#include <xil_cache.h>
#include <xbasic_types.h>
#include <ehw_component.h>
#include "genetic.h"

#define DEBUG_LEVEL 1

//Define the size of the LUT memories (in bytes)
#define LUT_MEMORY_SIZE 4096

//Define whether the monitoring of the timing performance of the various evolution flow parts is on
#define MONITOR_PERFORMANCE 0
#if MONITOR_PERFORMANCE
//Counter logic
XTmrCtr TimerCounter0;
unsigned int Value1;
unsigned int Value2;
#endif

//Fitness stopping criterias
#define END_ON_MAXIMUM_FITNESS 1
#define CONVERGED_SOLUTIONS_MIN_NUMBER 1

void memory_test();
void lut_test();

//Write data to the candidate solution input register
void ehwWrite(volatile int32 data0);
//Read the candidate solution output register
void ehwRead(int32 *data0);


//Write the candidate solution to the FPGA
int deployToFPGA(Candidate *candidate, unsigned int datum);

void fitness(int *fitness, int fitnessFunction, unsigned int fitnessMax, int verbose);

//Evaluate the stopping criteria
int resultFound(Population *pop, int fitnessMax);

#endif /*EHW_H_*/

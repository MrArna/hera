/*
 * @title Hera Project
 * @part Main
 * @author Davide Bartolini
 * @author Fabio Cancare
 * @author Matteo Carminati
 */
#include "ehw.h"

int main() {

	Xil_DCacheEnable();
	Xil_ICacheEnable();

	printf("\n\r\n\rStarting new evolutionary process\n\r\n\r");

	//EHW component test
	if(DEBUG_LEVEL >= 2) {
		printf("\n\r\n\rMemory testing\n\r\n\r");
		memory_test();
		printf("SR-LUT testing\n\r\n\r");
		lut_test();
		printf("Done\n\r\n\r");
	}

	//Counters
	int32 i, j, k, r, s;

	//Which fitness function has to be used
	int fitnessFunction = 2;

	//Optimal value of the fitness function
	unsigned int fitnessMax = 1024;

	//Seed for the RNG
	unsigned int seed;

	//Actual population
	Population *pop;
	static Population population;
	static Candidate candidatesPop[POPULATION_SIZE];

	//Next population
	Population *nextPop;
	static Population next_population;
	static Candidate candidatesNextPop[POPULATION_SIZE];
	Candidate *indCross1;
	Candidate *indCross2;

	int status;

	float convergency_level;

#if MONITOR_PERFORMANCE
	printf("Initializing the timer/counter\n\r");
	if (XTmrCtr_Initialize(&TimerCounter0, XPAR_XPS_TIMER_0_DEVICE_ID) != XST_SUCCESS) {
		printf("Timer init error\n\r");
		return -1;
	}
	if (XTmrCtr_SelfTest(&TimerCounter0, 0) != XST_SUCCESS) {
		printf("Timer self-test error\n\r");
		return -1;
	}
#endif


	for(seed = 0; seed < 20; seed++) {
		printf("\n\r\n\rFitness function: %d, RNG seed: %d\n\r\n\r", fitnessFunction , seed);
		if(MUTATION_LEVEL == MUTATION_MIXED_LEVEL) printf("Evolution type: MIXED\n\r");
		if(MUTATION_LEVEL == MUTATION_MULTIBIT_LEVEL) printf("Evolution type: MULTIBIT\n\r");
		if(MUTATION_LEVEL == MUTATION_MULTICELL_LEVEL) printf("Evolution type: FUNCTIONAL\n\r");
		printf("GA parameters: elitism: %f, mutation(level, p): (%d, %f), crossover(level, p): (%d, %f), selection prob.: %f\n\r", (float)ELITISM_PROB, MUTATION_LEVEL, MUTATION_PROB, CROSSOVER_LEVEL, CROSSOVER_PROB, SELECTION_FITTEST_PROB);

		if(DEBUG_LEVEL >= 2) printf("Instantiating memory for the populations\n\r");
		newPopulation(&pop, &population, candidatesPop);
		newPopulation(&nextPop, &next_population, candidatesNextPop);

		if(DEBUG_LEVEL >= 2) printf("Initializing the random seed\n\r");
		randomInit(seed);
		if(DEBUG_LEVEL >= 2) printf("DONE\n\r");
#if MONITOR_PERFORMANCE
		XTmrCtr_Start(&TimerCounter0, 0);
		Value1 = XTmrCtr_GetValue(&TimerCounter0, 0);
#endif
		if(DEBUG_LEVEL >= 2) printf("Initializing the first population\n\r");
		initPopulation(pop);
		if(DEBUG_LEVEL >= 2) printf("DONE\n\r");
#if MONITOR_PERFORMANCE
		Value2 = XTmrCtr_GetValue(&TimerCounter0, 0);
		XTmrCtr_Stop(&TimerCounter0, 0);
		XTmrCtr_Reset(&TimerCounter0, 0);
		printf("----- Pop init: %u -----\n\r", (Value2 - Value1));
#endif

		//Evaluate the fitness of the first population individuals and order them according to that
		for(j = 0; j < POPULATION_SIZE; j++) {
			if(DEBUG_LEVEL >= 2) {
				printf("Writing the individual number %d\n\r", j);
			}
#if MONITOR_PERFORMANCE == 1
			XTmrCtr_Start(&TimerCounter0, 0);
			Value1 = XTmrCtr_GetValue(&TimerCounter0, 0);
			status = deployToFPGA(pop->candidates[j]);
			Value2 = XTmrCtr_GetValue(&TimerCounter0, 0);
			XTmrCtr_Stop(&TimerCounter0, 0);
			XTmrCtr_Reset(&TimerCounter0, 0);
			printf("----- DeployToFPGA: %u -----\n\r", (Value2 - Value1));
#else
			status = deployToFPGA(pop->candidates[j], 0);
#endif
			if(status != XST_SUCCESS) {
				printf("Error while writing the bitstream\n\r");
				return -1;
			}

#if MONITOR_PERFORMANCE
			XTmrCtr_Start(&TimerCounter0, 0);
			Value1 = XTmrCtr_GetValue(&TimerCounter0, 0);
#endif
			fitness(&(pop->candidates[j]->fitness), fitnessFunction, fitnessMax, 0);

#if MONITOR_PERFORMANCE
			Value2 = XTmrCtr_GetValue(&TimerCounter0, 0);
			XTmrCtr_Stop(&TimerCounter0, 0);
			XTmrCtr_Reset(&TimerCounter0, 0);
			printf("----- Fitness evaluation: %u -----\n\r", (Value2 - Value1));
#endif
		}
#if MONITOR_PERFORMANCE
		XTmrCtr_Start(&TimerCounter0, 0);
		Value1 = XTmrCtr_GetValue(&TimerCounter0, 0);
#endif
		ordering(pop, 0, POPULATION_SIZE - 1);
		convergency_level = (float)pop->candidates[0]->fitness/(float)fitnessMax;
#if MONITOR_PERFORMANCE
			Value2 = XTmrCtr_GetValue(&TimerCounter0, 0);
			XTmrCtr_Stop(&TimerCounter0, 0);
			XTmrCtr_Reset(&TimerCounter0, 0);
			printf("----- Population ordering: %u -----\n\r", (Value2 - Value1));
#endif
		if(DEBUG_LEVEL >= 1) {
			printf("Initial, ordered, population fitness:");
			for(j = 0; j < POPULATION_SIZE; j++) {
				printf(" %d", pop->candidates[j]->fitness);
			}
			printf("\n\r\n\r");
		}

		//Evolutionary cycle, stops when the overall population fitness achieve
		//the specified result or when MAX_GENERATION_NUMBER generations were created
		for(i = 1; i <= MAX_GENERATION_NUMBER && !resultFound(pop, fitnessMax); i++) {
			nextPop->generation_number = i;
#if MONITOR_PERFORMANCE
			XTmrCtr_Start(&TimerCounter0, 0);
			Value1 = XTmrCtr_GetValue(&TimerCounter0, 0);
#endif
			for(j = 0; j < POPULATION_SIZE - 1 - (int)(ELITISM_PROB * (float)POPULATION_SIZE); j = j + 2) {
				//Select the two candidate solutions (parents) for creating two offsprings
				tournamentSelection(pop, &indCross1, &indCross2);

				//Eventually perform crossover
				crossover(indCross1, indCross2, nextPop->candidates[j], nextPop->candidates[j + 1], CROSSOVER_PROB, CROSSOVER_LEVEL);

				//Eventually perform mutation on the first offspring
				mutation(nextPop->candidates[j], MUTATION_PROB, MUTATION_LEVEL, convergency_level);

				//Eventually perform mutation on the second offspring
				mutation(nextPop->candidates[j + 1], MUTATION_PROB, MUTATION_LEVEL, convergency_level);
			}

			//Elitism
			for(k = 0; k < (int)(ELITISM_PROB * (float)POPULATION_SIZE) && j < POPULATION_SIZE; k++) {
				if(DEBUG_LEVEL >= 2) {
					printf("Elitism: k: %d, j: %d, POP_SIZE: %d\n\r",k,j,POPULATION_SIZE);
				}
				copyCandidate(nextPop->candidates[j], pop->candidates[k], 0, PARTS_PER_CANDIDATE_SOLUTION, 0, MODULES_PER_PART, 0, CELLS_PER_MODULE);
				j++;
				if(DEBUG_LEVEL >= 2) {
					printf("Candidate solution %d with fitness: %d has been copied to the next population (elitism)\n\r", k, pop->candidates[k]->fitness);
					if(DEBUG_LEVEL >= 3) {
						printCandidateSolution(j, i, nextPop->candidates[j]);
					}
				}
			}

			//Preserve population cardinality
			while(j < POPULATION_SIZE) {
				if(DEBUG_LEVEL >= 2) {
					printf("Warning: preservation needed\n\r\n\r");
				}
				//Selection
				tournamentSelection(pop, &indCross1, &indCross2);
				//Crossover
				crossover(indCross1, indCross2, nextPop->candidates[j], nextPop->candidates[j], CROSSOVER_PROB, CROSSOVER_LEVEL);
				//Mutation
				mutation(nextPop->candidates[j], MUTATION_PROB, MUTATION_LEVEL, convergency_level);
				j++;
			}
#if MONITOR_PERFORMANCE
					Value2 = XTmrCtr_GetValue(&TimerCounter0, 0);
					XTmrCtr_Stop(&TimerCounter0, 0);
					XTmrCtr_Reset(&TimerCounter0, 0);
					printf("----- Genetic algorithm (all individuals): %u -----\n\r", (Value2 - Value1));
#endif

			//Compute the fitness of the new candidate solutions
			for(j = 0; j < POPULATION_SIZE; j++) {
				//Write the candidate solution bitstream to the evolvable area
#if MONITOR_PERFORMANCE
				XTmrCtr_Start(&TimerCounter0, 0);
				Value1 = XTmrCtr_GetValue(&TimerCounter0, 0);
				status = deployToFPGA(nextPop->candidates[j]);
				Value2 = XTmrCtr_GetValue(&TimerCounter0, 0);
				XTmrCtr_Stop(&TimerCounter0, 0);
				XTmrCtr_Reset(&TimerCounter0, 0);
				printf("----- DeployToFPGA: %u -----\n\r", (Value2 - Value1));
#else
				status = deployToFPGA(nextPop->candidates[j], 0);
#endif
				if(status != XST_SUCCESS) {
					printf("Error while writing the bitstream\n\r");
					return -1;
				}
#if MONITOR_PERFORMANCE
				XTmrCtr_Start(&TimerCounter0, 0);
				Value1 = XTmrCtr_GetValue(&TimerCounter0, 0);
#endif

				fitness(&(nextPop->candidates[j]->fitness), fitnessFunction, fitnessMax, 0);
#if MONITOR_PERFORMANCE
				Value2 = XTmrCtr_GetValue(&TimerCounter0, 0);
				XTmrCtr_Stop(&TimerCounter0, 0);
				XTmrCtr_Reset(&TimerCounter0, 0);
				printf("----- Fitness evaluation: %u -----\n\r", (Value2 - Value1));
#endif
			}
			//Order the new candidate solutions according to their fitness
#if MONITOR_PERFORMANCE
			XTmrCtr_Start(&TimerCounter0, 0);
			Value1 = XTmrCtr_GetValue(&TimerCounter0, 0);
#endif
			ordering(nextPop, 0, POPULATION_SIZE - 1);
			convergency_level = (float)pop->candidates[0]->fitness/(float)fitnessMax;
#if MONITOR_PERFORMANCE
			Value2 = XTmrCtr_GetValue(&TimerCounter0, 0);
			XTmrCtr_Stop(&TimerCounter0, 0);
			XTmrCtr_Reset(&TimerCounter0, 0);
			printf("----- Population ordering: %u -----\n\r", (Value2 - Value1));
#endif

			//Check whether the fitness of the best candidate solution within the new population is smaller than the one of the old population
			if(DEBUG_LEVEL >= 2) {
				if(nextPop->candidates[0]->fitness < pop->candidates[0]->fitness) {
					printf("\n\rThe maximum fitness has been decreased\n\r");
				}
			}

			switchPopulation(&nextPop, &pop);
			if(!(i % 50)) {
				printf("Generation #%d, fitness:", i);
				for(j = 0; j < POPULATION_SIZE; j++) {
					printf(" %d", pop->candidates[j]->fitness);
					if(DEBUG_LEVEL >= 3 && !(i % 100)) {
						printf("\n\r");
						printf("########Individual %d Generation %d (LUT content)########\n\r", j, i);
						printf("fitness: %d\n\r", pop->candidates[j]->fitness);
						for(k = 0; k < PARTS_PER_CANDIDATE_SOLUTION; k++) {
							for(r = 0; r < MODULES_PER_PART; r++) {
								for(s = 0; s < CELLS_PER_MODULE; s++) {
									printf("0x%X", pop->candidates[j]->parts[k].modules[r].cells[s].luts);
									if(r < MODULES_PER_PART - 1 || s < CELLS_PER_MODULE - 1) {
										printf(", ");
									}
								}
							}
							printf("\n\r");
						}
					}
				}
				printf("\n\r\n\r");
			}
#if MONITOR_PERFORMANCE
					Value2 = XTmrCtr_GetValue(&TimerCounter0, 0);
					XTmrCtr_Stop(&TimerCounter0, 0);
					XTmrCtr_Reset(&TimerCounter0, 0);
					printf("----- Generation time: %u -----\n\r", (Value2 - Value1));
#endif
		}
		if(i >= MAX_GENERATION_NUMBER) {
			printf("Perfect solution not found\n\r\n\r");
		}
	}
	printf("######## END #######\n\r");
	Xil_DCacheDisable();
	Xil_ICacheDisable();
	return XST_SUCCESS;

}

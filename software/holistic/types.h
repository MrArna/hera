#ifndef TYPES_H
#define TYPES_H

#include <stdlib.h>
#include <string>

#include <tools.h>

template <size_t CS>
struct cell_f
{
	std::bitset<CS * CS> tt;
	int weight;
};

template <size_t CS>
class Cell
{
	private:
		std::bitset<CS * CS> tt;

	public:
		Cell() : tt(0)
		{
		}

		Cell(unsigned long v)
		{
			std::bitset<CS * CS> tmp(v);
			tt = tmp;
		}

		Cell(const Cell<CS> &c)
		{
			for (int i = 0; i < tt.size(); i++)
					tt.set(i, c.test(i));
		}

		Cell(const char *v)
		{
			tt = strtobitset<CS>(v);
		}

		size_t size()
		{
			return tt.size();
		}

		bool test(int i) const
		{
			return tt.test(i);
		}

		void set(int i)
		{
			tt.set(i);
		}

		void set(const std::bitset<CS * CS> &new_tt)
		{
			tt = new_tt;
		}

		void reset(int i)
		{
			tt.reset(i);
		}

		const std::bitset<CS * CS> &gettt() const
		{
			return tt;
		}

		void randinit(cell_f<CS> *pool)
		{
			if (!pool)
				for (int i = 0; i < tt.size(); i++)
						tt.set(i, rand() % 2);
			else
				tt = pool[rand() % sizeof(*pool)].tt;
		}

		const std::string to_string() const
		{
			return tt.to_string();
		}
};


template <size_t N, size_t CS>
struct col_routing
{
	int from[N][CS];
};

template <size_t N, size_t M, size_t CS>
class Individual
{
	private:
		Cell<CS> cells[M][N];
		col_routing<N, CS> cr[N];

		void updatecr(int m, const col_routing<N, CS> &new_cr)
		{
			for (int j = 0; j < N; j++)
				for (int k = 0; k < CS; k++)
					cr[m].from[j][k] = new_cr.from[j][k];
		}

	public:
		const size_t width;
		const size_t height;
		const size_t cell_size;

		Individual(const col_routing<N, CS> *c_r[N])
			: width(M), height(N), cell_size(CS * CS)
		{
			for (int i = 0; i < M; i++)
				for (int j = 0; j < N; j++)
					for (int k = 0; k < CS; k++)
						cr[i].from[j][k] =
							c_r[i]->from[j][k];
		}

		Individual(const Individual<N, M, CS> &parent)
			: width(M), height(N), cell_size(CS * CS)
		{
			for (int i = 0; i < M; i++) {
				updatecr(i, parent.getcr(i));
				for (int j = 0; j < N; j++)
					setcell(i, j, parent.getcell(i, j));
			}
		}

		const col_routing<N, CS> &getcr(int m) const
		{
			return cr[m];
		}

		const Cell<CS> &getcell(int m, int n) const
		{
			return cells[m][n];
		}

		void setcell(int m, int n, const Cell<CS> &c)
		{
			cells[m][n] = c;
		}

		void init(const std::bitset<CS * CS> &tt)
		{
			for (int i = 0; i < M; i++)
				for(int j = 0; j < N; j++)
					cells[i][j].set(tt);

		}
		void randinit(cell_f<CS> *pool)
		{
			for (int i = 0; i < M; i++)
				for(int j = 0; j < N; j++)
					cells[i][j].randinit(pool);
		}

		const std::string to_string() const
		{
			std::string str;

			for (int i = 0; i < M; i++) {
				for(int j = 0; j < N; j++) {
					str.append(cells[i][j].to_string());
					str.append(" ");
				}
				str.append("\n\r");
			}

			return str;
		}
};

#endif /* TYPES_H */


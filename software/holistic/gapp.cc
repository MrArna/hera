#include <stdio.h>

#include <tools.h>
#include <genetic.h>
#include <arch.h>

void init()
{
	__init();
}

int main (void)
{
	init();

	test();

	cleanup();

	return 0;
}


#include <stdio.h>

#include <genetic.h>

cell4_f cell4_pool[] =
{
	{0x8000, 1}, // and
	{0xFFFE, 1}, // or
	{0x6996, 1}, // xor
	{0xFFFF, 1}, // set
	{0x0000, 1}, //reset
	{0x5555, 1}, // even numbers
	{0xAAAA, 1}, // odd numbers | forward input 0
	{0x28AE, 1}, // prime numbers
	{0x0213, 1}, // square numbers
	{0x00FF, 1}, // numbers less than 8
	{0xFF00, 1}, // numbers greater or equal than 8 | forward in 3
	{0x212F, 1}, // fibonacci sequence
	{0x844A, 1}, // triangular numbers
	{0xCCCC, 1}, // forward input 1
	{0xF0F0, 1}, // forward input 2
	{(int16_t) (0xFFFF - 0x8000), 1}, // not and
	{(int16_t) (0xFFFF - 0xFFFE), 1}, // not or
	{(int16_t) (0xFFFF - 0x6996), 1}, // not xor
	{(int16_t) (0xFFFF - 0x28AE), 1}, // not prime numbers
	{(int16_t) (0xFFFF - 0x0213), 1}, // not square numbers
	{(int16_t) (0xFFFF - 0x212F), 1}, // not fibonacci sequence
	{(int16_t) (0xFFFF - 0x844A), 1}, // not triangular numbers
	{(int16_t) (0xFFFF - 0xCCCC), 1}, // not forward input 1
	{(int16_t) (0xFFFF - 0xF0F0), 1}, // not forward input 2
	{(int16_t) (0xFFFF - 0xAAAA), 1}, // not odd numbers | forward input 0
	{(int16_t) (0xFFFF - 0xFF00), 1}, // not numbers greater or equal than 8 | forward in 3
};

const col_routing_ind8 cr0_ind8 =
{
	{
		{3, 2, 1, 0},
		{7, 6, 5, 4},
		{3, 2, 1, 0},
		{7, 6, 5, 4},
		{3, 2, 1, 0},
		{7, 6, 5, 4},
		{3, 2, 1, 0},
		{7, 6, 5, 4}
	}
};

const col_routing_ind32 cr0i_ind32 =
{
	{
		{3, 2, 1, 0},
		{7, 6, 5, 4},
		{3, 2, 1, 0},
		{7, 6, 5, 4},
		{3, 2, 1, 0},
		{7, 6, 5, 4},
		{3, 2, 1, 0},
		{7, 6, 5, 4},

		{11, 10, 9, 8},
		{15, 14, 13, 12},
		{11, 10, 9, 8},
		{15, 14, 13, 12},
		{11, 10, 9, 8},
		{15, 14, 13, 12},
		{11, 10, 9, 8},
		{15, 14, 13, 12},

		{19, 18, 17, 16},
		{23, 22, 21, 20},
		{19, 18, 17, 16},
		{23, 22, 21, 20},
		{19, 18, 17, 16},
		{23, 22, 21, 20},
		{19, 18, 17, 16},
		{23, 22, 21, 20},

		{27, 26, 25, 24},
		{31, 30, 29, 28},
		{27, 26, 25, 24},
		{31, 30, 29, 28},
		{27, 26, 25, 24},
		{31, 30, 29, 28},
		{27, 26, 25, 24},
		{31, 30, 29, 28}
	}
};

const col_routing_ind32 cr0m_ind32 =
{
	{
		{23, 22, 31, 30},
		{7, 6, 15, 14},
		{23, 22, 31, 30},
		{7, 6, 15, 14},
		{23, 22, 31, 30},
		{7, 6, 15, 14},
		{23, 22, 31, 30},
		{7, 6, 15, 14},

		{21, 20, 29, 28},
		{5, 4, 13, 12},
		{21, 20, 29, 28},
		{5, 4, 13, 12},
		{21, 20, 29, 28},
		{5, 4, 13, 12},
		{21, 20, 29, 28},
		{5, 4, 13, 12},

		{19, 18, 27, 26},
		{3, 2, 11, 10},
		{19, 18, 27, 26},
		{3, 2, 11, 10},
		{19, 18, 27, 26},
		{3, 2, 11, 10},
		{19, 18, 27, 26},
		{3, 2, 11, 10},

		{17, 16, 25, 24},
		{1, 0, 9, 8},
		{17, 16, 25, 24},
		{1, 0, 9, 8},
		{17, 16, 25, 24},
		{1, 0, 9, 8},
		{17, 16, 25, 24},
		{1, 0, 9, 8}
	}
};

const col_routing_ind32 *cr0_ind32[16] = {
	&cr0i_ind32,
	&cr0i_ind32,
	&cr0i_ind32,
	&cr0i_ind32,
	&cr0m_ind32,
	&cr0i_ind32,
	&cr0i_ind32,
	&cr0i_ind32,
	&cr0m_ind32,
	&cr0i_ind32,
	&cr0i_ind32,
	&cr0i_ind32,
	&cr0m_ind32,
	&cr0i_ind32,
	&cr0i_ind32,
	&cr0i_ind32
};


#!/bin/bash

make mb || exit -1
echo "Update bitstream? [y/_]"
read answ
if [ "$answ" == "y" ]; then
	pushd ../..
	make -f system.make download || (exit -1; popd)
	popd
fi
xmd -tcl 'tcl/run.tcl' || exit -1

echo "Running.."
echo "Hit Enter to stop"
read

xmd -tcl 'tcl/stop.tcl' || exit -1


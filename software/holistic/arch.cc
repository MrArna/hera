#include <stdio.h>
#include <limits.h>

#include <const.h>
#include <genetic.h>
#include <arch.h>
#include <assert.h>

template <size_t N, size_t M, size_t CS>
std::bitset<N>
evaluate(const Individual<N, M, CS> &ind, const std::bitset<N> &input);

void __test_base()
{
	printf("\n\rTest ind32..\n\r");
	std::bitset<32> input(0xFFFFFFFF);
	Ind32 ind(cr0_ind32);
	//ind.randinit(cell4_pool);
	ind.init(std::bitset<16>(0xFFFE));

	std::bitset<32> res1 = evaluate(ind, 0);
	printf("iotest -- %s\n\r", res1.to_string().data());
	std::bitset<32> res2 = evaluate(ind, 1);
	printf("iotest -- %s\n\r", res2.to_string().data());
	assert(res1.to_ulong() == 0 && res2.to_ulong() == 0xFFFFFFFF);

	for (int i = 0; i < 8; i++) {
		ind.setcell(14, i * 4 + 0, Cell<4>("0000"));
		ind.setcell(14, i * 4 + 1, Cell<4>("0000"));
		ind.setcell(14, i * 4 + 2, Cell<4>("0000"));
		ind.setcell(14, i * 4 + 3, Cell<4>("0000"));
		std::bitset<32> res3 = evaluate(ind, 1);
		printf("%d -- %s\n\r", i, res3.to_string().data());
	}
}

#if EHW_ARCH == EHW_ARCH_SW
#include <time.h>

void __init()
{
	//FIXME Add proper random init here
	srand(37);
}

void __perfeval();
void test()
{
	__test_base();
	__perfeval();
}

void cleanup()
{
}

template <size_t CS>
bool eval_cell(const Cell<CS> &c, const std::bitset<CS> &in)
{
	//FIXME assuming input never overflows long int
	return c.test(in.to_ulong());
}


template <size_t N, size_t M, size_t CS>
std::bitset<N>
evaluate(const Individual<N, M, CS> &ind, const std::bitset<N> &input)
{
	std::bitset<N> buf = input;
	std::bitset<N> tmp_out;
	for (int i = 0; i < M; i++) {
		tmp_out.reset();
		for(int j = 0; j < N; j++) {
			std::bitset<CS> tmp_in(0);
			for (int k = 0; k < CS; k++) {
				tmp_in.set(k, buf.test(
					ind.getcr(i).from[j][k]));
			}
			tmp_out.set(j, eval_cell(ind.getcell(i, j), tmp_in));
//printf("(%d %d) %s\n\r", i, j, tmp_out.to_string().data());
		}
		buf = tmp_out;
	}

	return tmp_out;
}

inline std::bitset<8> evaluate(const Ind8 &ind, const std::bitset<8> &input)
{
	return evaluate<8, 4, 4>(ind, input);
}

inline std::bitset<32> evaluate(const Ind32 &ind, const std::bitset<32> &input)
{
	return evaluate<32, 16, 4>(ind, input);
}
void __perfeval()
{
	struct timespec ts;
	double t0, t1;

	printf("Performance evaluation..\n\r");
	Ind32 ind(cr0_ind32);
	ind.randinit(cell4_pool);
	//ind.init(std::bitset<16>(0xFFFE));

	clock_gettime(CLOCK_MONOTONIC, &ts);
	t0 = (double) (ts.tv_sec * 1000000000 + ts.tv_nsec) / 1000.0;
	std::bitset<32> res;
	int j;
	for (j = 0; j < 4096; j ++) {
		std::bitset<32> i(j);
		res = evaluate(ind, i);
	}
	clock_gettime(CLOCK_MONOTONIC, &ts);
	t1 = (double) (ts.tv_sec * 1000000000 + ts.tv_nsec) / 1000.0;
	double tio = t1 - t0;
	printf("%d evaluations (16 cols) (%s)\n\r\t%fus\n\r",
					j, res.to_string().data(), tio);

	return;
}


#elif EHW_ARCH == EHW_ARCH_IND32X16_LUT4_R0
#include <xil_cache.h>
#include <xparameters.h>
#include <xbasic_types.h>
#include <xil_io.h>
#include <xstatus.h>
#include <xtmrctr.h>

#define TIMER_ID XPAR_XPS_TIMER_0_CLOCK_FREQ_HZ
#define TIMER_FREQ XPAR_XPS_TIMER_0_CLOCK_FREQ_HZ

#define BASEADDR (XPAR_IND32X16_LUT4_R0_0_BASEADDR)
#define INREG (BASEADDR + 0x0)
#define OUTREG (BASEADDR + 0x4)
#define RECCTRLREG (BASEADDR + 0x8)
#define EVCTRLREG (BASEADDR + 0xC)
#define CHECKREG (BASEADDR + 0x10)

#define LUTCFGMASK 0xFFFF0000
#define COLADDRMASK 0x000001E0
#define CELLADDRMASK 0x0000001F

#define RECONFMASK 0x00000200
#define RECACKMASK 0x40000000
#define CONFVALIDMASK 0x00000001

#define EVALMASK 0x80000000
#define OUTCOLMASK 0x78000000
#define IOREADYMASK 0x80000000

#define DEPLOY_TIMEOUT 1000000

void __init()
{
	XTmrCtr tmr;

	XTmrCtr_Initialize(&tmr, XPAR_XPS_TIMER_0_DEVICE_ID);
	XTmrCtr_SetResetValue(&tmr, 0, 0);
	XTmrCtr_Reset(&tmr, 0);
	XTmrCtr_Start(&tmr, 0);
	xil_printf("Initializing random seed; press enter "
				"when you feel like doing it :-)\n\r");
	getchar();
	XTmrCtr_Stop(&tmr, 0);
	int seed = XTmrCtr_GetValue(&tmr, 0) % RAND_MAX;
	srand(seed);
	xil_printf("Done, seed: %d..\n\r", seed);

	Xil_DCacheEnable();
	Xil_ICacheEnable();

	Xil_Out32(EVCTRLREG, 0);
	Xil_Out32(RECCTRLREG, 0);
}

void __perfeval();
void test()
{
	__test_base();
	__perfeval();
}

void cleanup()
{
	Xil_DCacheDisable();
	Xil_ICacheDisable();
}

std::bitset<8> evaluate(const Ind8 &ind, const std::bitset<8> &input)
{
	fprintf(stderr, "Error, 8 bit individual not supported in this arch\n\r");
	return std::bitset<8>(0);
}

int deploy(const Individual<32, 16, 4> &ind) {

	for (int i = 0; !(Xil_In32(CHECKREG) & CONFVALIDMASK); i++)
		if (i > DEPLOY_TIMEOUT)
			return XST_FAILURE;

	Xil_Out32(RECCTRLREG, 0);
	for(int i = 0; i < 16; i++) {
		for(int j = 0; j < 32; j++) {
			Xuint32 caddr, addr, val;
			caddr = j & CELLADDRMASK;
			addr = i;
			addr = ((addr << 5) & COLADDRMASK) | caddr;
			val = ind.getcell(i, j).gettt().to_ulong();
			val = (val << 16) & LUTCFGMASK;
			const Xint32 ctrlreg = val | addr | RECONFMASK;
			Xil_Out32(RECCTRLREG, ctrlreg);
			while (!(Xil_In32(CHECKREG) & RECACKMASK)) {
				xil_printf("(%d, %d) Waiting ack (%08X)..\n\r",
						i, j, Xil_In32(CHECKREG));
				xil_printf("\tRECCTRLREG : %08X\n\r",
							Xil_In32(RECCTRLREG));
			}
			Xil_Out32(RECCTRLREG, 0);
			//Xil_Out32(RECCTRLREG, ctrlreg & (!RECONFMASK));
		}
	}

	return XST_SUCCESS;
}

Xint32 __indio(Xint32 data, Xint32 outcol)
{
	const Xint32 outcol__ = (outcol << 27) & OUTCOLMASK;
	const Xint32 ctrlreg = EVALMASK | (outcol__ & OUTCOLMASK);

// Assuming that when this function is called, the configuration is valid
/*	while (!(Xil_In32(CHECKREG) & CONFVALIDMASK))
		xil_printf("Waiting conf validity (%08X)..\n\r", Xil_In32(CHECKREG));
	*/

	Xil_Out32(INREG, data);

// Assuming that the eval ctrl bit has already been cleaned
//	Xil_Out32(EVCTRLREG, 0);
	Xil_Out32(EVCTRLREG, ctrlreg);

	while (!(Xil_In32(CHECKREG) & IOREADYMASK))
		xil_printf("Waiting res validity (%08X)..\n\r", Xil_In32(CHECKREG));

	Xil_Out32(EVCTRLREG, 0);

	return Xil_In32(OUTREG);
}

std::bitset<32> evaluate(const Ind32 &ind, const std::bitset<32> &input)
{
	int ret;

	ret = deploy(ind);
	if (ret != XST_SUCCESS) {
		fprintf(stderr, "Error during individual deployment\n\r");
		return std::bitset<32>(0);
	}

	Xint32 res = __indio((Xint32) input.to_ulong(), 15);

	return std::bitset<32>((unsigned long) res);
}

void __perfeval()
{
	XTmrCtr tmr;

	XTmrCtr_Initialize(&tmr, XPAR_XPS_TIMER_0_DEVICE_ID);
	XTmrCtr_SetResetValue(&tmr, 0, 0);

	xil_printf("Performance evaluation..\n\r");
	Ind32 ind(cr0_ind32);
	ind.randinit(cell4_pool);
	//ind.init(std::bitset<16>(0xFFFE));
	XTmrCtr_Reset(&tmr, 0);
	XTmrCtr_Start(&tmr, 0);
	deploy(ind);
	XTmrCtr_Stop(&tmr, 0);
	double tdepl = (double)XTmrCtr_GetValue(&tmr, 0) * 1000000 /
							(double)TIMER_FREQ;
	printf("Deployment: \n\r\t%fus\n\r", tdepl);

	for (int i = 15; i >= 0; i -= 4) {
		int j;
		Xint32 in, res;
		XTmrCtr_Reset(&tmr, 0);
		XTmrCtr_Start(&tmr, 0);
		for (j = 0; j < 4096; j ++) {
			in = j;
			res = __indio(in, i);
		}
		XTmrCtr_Stop(&tmr, 0);
		double tio = (double)XTmrCtr_GetValue(&tmr, 0) * 1000000 /
							(double)TIMER_FREQ;
		printf("%d evaluations (%d cols) (%08X -> %08X)\n\r\t%fus\n\r",
						j, i + 1, in, res, tio);
	}

	return;
}
#endif


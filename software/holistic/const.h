#ifndef CONST_H
#define CONST_H

#define EHW_ARCH_SW 0
#define EHW_ARCH_IND32X16_LUT4_R0 1

#ifndef EHW_ARCH
#warning "EHW_ARCH undefined; falling back to EHW_ARCH_SW"
#define EHW_ARCH ARCH_SW
#endif

#endif /* CONST_H */


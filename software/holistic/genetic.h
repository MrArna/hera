#ifndef GENETIC_H
#define GENETIC_H

#include <stdint.h>
#include <bitset>

#include <tools.h>
#include <types.h>

typedef Cell<4> Cell4;
typedef cell_f<4> cell4_f;
extern cell4_f cell4_pool[];

typedef col_routing<8, 4> col_routing_ind8;
extern const col_routing_ind8 cr0_ind8;
typedef Individual<8, 4, 4> Ind8;

typedef col_routing<32, 4> col_routing_ind32;
extern const col_routing_ind32 cr0i_ind32;
extern const col_routing_ind32 cr0m_ind32;
extern const col_routing_ind32 *cr0_ind32[16];
typedef Individual<32, 16, 4> Ind32;


#endif /* GENETIC_H */


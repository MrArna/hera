#ifndef ARCH_H
#define ARCH_H

#include <bitset>

#include <genetic.h>

void __init();
void cleanup();
void test();

std::bitset<8> evaluate(const Ind8 &ind, const std::bitset<8> &input);
std::bitset<32> evaluate(const Ind32 &ind, const std::bitset<32> &input);

#endif /* ARCH_H */


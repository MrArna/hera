#ifndef TOOLS_H
#define TOOLS_H

#include <string>
#include <string.h>
#include <bitset>
#include <stdio.h>

template <size_t N>
void setnibble(std::bitset<N> *bs, int start, bool v0, bool v1, bool v2, bool v3)
{
	bs->set(start + 3, v0);
	bs->set(start + 2, v1);
	bs->set(start + 1, v2);
	bs->set(start, v3);
}

template <size_t N>
const std::bitset<N * N> strtobitset(const char *str)
{
	std::bitset<N * N> bs;

	for (int i = 0; i < strlen(str); i++) {
		int pos = (strlen(str) - 1 - i) * 4;
		switch (str[i]) {
			case '0':
				setnibble(&bs, pos, 0, 0, 0, 0);
				break;
			case '1':
				setnibble(&bs, pos, 0, 0, 0, 1);
				break;
			case '2':
				setnibble(&bs, pos, 0, 0, 1, 0);
				break;
			case '3':
				setnibble(&bs, pos, 0, 0, 1, 1);
				break;
			case '4':
				setnibble(&bs, pos, 0, 1, 0, 0);
				break;
			case '5':
				setnibble(&bs, pos, 0, 1, 0, 1);
				break;
			case '6':
				setnibble(&bs, pos, 0, 1, 1, 0);
				break;
			case '7':
				setnibble(&bs, pos, 0, 1, 1, 1);
				break;
			case '8':
				setnibble(&bs, pos, 1, 0, 0, 0);
				break;
			case '9':
				setnibble(&bs, pos, 1, 0, 0, 1);
				break;
			case 'A':
				setnibble(&bs, pos, 1, 0, 1, 0);
				break;
			case 'B':
				setnibble(&bs, pos, 1, 0, 1, 1);
				break;
			case 'C':
				setnibble(&bs, pos, 1, 1, 0, 0);
				break;
			case 'D':
				setnibble(&bs, pos, 1, 1, 0, 1);
				break;
			case 'E':
				setnibble(&bs, pos, 1, 1, 1, 0);
				break;
			case 'F':
				setnibble(&bs, pos, 1, 1, 1, 1);
				break;
			default:
				fprintf(stderr, "Invalid HEX char %c\n", str[i]);
		}
	}

	return bs;
}

#endif /* TOOLS_H */


/*
 * @title Hera Project
 * @part Main
 * @author Davide Bartolini
 * @author Fabio Cancare
 * @author Matteo Carminati
 */
#include "icap.h"
#include "ehw.h"
#include "xcache_l.h"
#include "xil_cache.h"

#define FW3 0xFF00FF00
#define FW2 0xF0F0F0F0
#define FW1 0xCCCCCCCC
#define FW0 0xAAAAAAAA
#define LUT_VALUE FW3

void printCandidateSolutionBitstream(int individual_number, int generation_number, Candidate* candidate, XHwIcap HwIcap) {
	printf("######## Candidate solution %d bitstream, Gen: %d, deployed from %d, %d to %d, %d ########\n", individual_number, generation_number, X[0] , Y[0], X[PARTS_PER_CANDIDATE_SOLUTION-1] , Y[PARTS_PER_CANDIDATE_SOLUTION-1]);
	printf("Fitness: %d\n", candidate->fitness);
	printFrame(&HwIcap, X[0], Y[0]);
	printf("######## End ########\n");
}

int main() {
	int32 input = 0;
	int32 output = 0;

	XCache_EnableICache(0x80000001);
	XCache_EnableDCache(0x80000001);

	unsigned int i, j, k;
	int r, s;


	static XHwIcap HwIcap;
	//Actual population
	static Candidate candidate;

	int status;


	if(DEBUG_LEVEL >= 2) printf("Initializing the ICAP controller\n");
	//ICAP initialization
	if(initIcap(&HwIcap) == XST_FAILURE) {
		printf("ICAP init error\n\r");
		return -1;
	}
	printf("\n\n");

//Per far passare bit
	for(i = 0; i < 16; i++) {
		for(j = 0; j < 4; j++) {
			for(k = 0; k < 8; k++) {
				candidate.parts[i].modules[j].cells[k].luts = 0;
			}
		}
	}

//	candidate.parts[8].modules[3].cells[0].luts = 0xFFFFFFFF;

//	candidate.parts[15].modules[0].cells[1].luts = FW3;
//	candidate.parts[15].modules[1].cells[4].luts = FW3;
	candidate.parts[15].modules[2].cells[1].luts = 0xFFFFFFFF;
	for(i = 0; i < 8; i++) {
		candidate.parts[15].modules[3].cells[i].luts = 0x40004000;
	}
	printf("%08X\n", candidate.parts[15].modules[3].cells[0].luts);

	status = deployToFPGA(&HwIcap, &candidate, X, Y);
	input = 0x00000000;
	ehwWrite(input);
	output = ehwRead();
	printf("input_%d-%d-%d: %08X\tout: %08X\n\r",
		i, j, k, input, output);
//	candidate.parts[12 + j].modules[3].cells[k].luts = 0;

//Per quarta colonna (input 00)
/*	printf("LUT configuration: %08X\n\r", LUT_VALUE);
	for(i = 0; i < MODULES_PER_PART; i++){
		for(j = 0; j < CELLS_PER_MODULE; j++){
			candidate.parts[0].modules[i].cells[j].luts = 0;
		}
	}
	for(i = 0; i < 8; i++) {
		candidate.parts[0].modules[3].cells[i].luts = LUT_VALUE;
	}
	for(i = 0; i < 8; i++){
		candidate.parts[0].modules[2].cells[i].luts = 0xFFFFFFFF;
	status = deployToFPGA(&HwIcap, &candidate, X, Y);
		ehwWrite(input);
		output = ehwRead();
		printf("input_%d:\t%02X\t\
			4output:\t%02X\n\r",
			i, (unsigned char)(output >> 16), (unsigned char)(output >> 24));
		candidate.parts[0].modules[2].cells[i].luts = 0x00000000;
	}
*/
//Per terza colonna (input 00)
/*	printf("LUT configuration: %08X\n\r", LUT_VALUE);
	for(i = 0; i < MODULES_PER_PART; i++){
		for(j = 0; j < CELLS_PER_MODULE; j++){
			candidate.parts[0].modules[i].cells[j].luts = 0;
		}
	}
	for(i = 0; i < 8; i++) {
		candidate.parts[0].modules[2].cells[i].luts = LUT_VALUE;
	}
	for(i = 0; i < 8; i++){
		candidate.parts[0].modules[1].cells[i].luts = 0xFFFFFFFF;
		status = deployToFPGA(&HwIcap, &candidate, X, Y);
		ehwWrite(input);
		output = ehwRead();
		printf("input_%d:\t%02X\t\
			3output:\t%02X\n\r",
			i, (unsigned char)(output >> 8), (unsigned char)(output >> 16));
		candidate.parts[0].modules[1].cells[i].luts = 0x00000000;
	}
*/
//Per seconda colonna (input 00)
/*	printf("LUT configuration: %08X\n\r", LUT_VALUE);
	for(i = 0; i < MODULES_PER_PART; i++){
		for(j = 0; j < CELLS_PER_MODULE; j++){
			candidate.parts[0].modules[i].cells[j].luts = 0;
		}
	}
	for(i = 0; i < 8; i++) {
		candidate.parts[0].modules[1].cells[i].luts = LUT_VALUE;
	}
	for(i = 0; i < 8; i++){
		candidate.parts[0].modules[0].cells[i].luts = 0xFFFFFFFF;
		status = deployToFPGA(&HwIcap, &candidate, X, Y);
		ehwWrite(input);
		output = ehwRead();
		printf("input_%d:\t%02X\t\
			2output:\t%02X\n\r",
			i, (unsigned char)(output), (unsigned char)(output >> 8));
		candidate.parts[0].modules[0].cells[i].luts = 0x00000000;
	}
*/

//Per prima colonna
/*	printf("LUT configuration: %08X\n\r", LUT_VALUE);
	for(i = 0; i < MODULES_PER_PART; i++){
		for(j = 0; j < CELLS_PER_MODULE; j++){
			candidate.parts[0].modules[i].cells[j].luts = LUT_VALUE;
		}
	}

	status = deployToFPGA(&HwIcap, &candidate, X, Y);

	input = 0x01;
	input = input << 24;

	for(i = 0; i < 8; i++){
		ehwWrite(input);
		output = ehwRead();
		printf("input_%d:\t%08X\t\
			1output:\t%02X\n\r",
			i, input, (unsigned char)(output));
		input = input << 1;
	}
*/

/*
	printf("input:\t%08X\n\r\
		1output:\t%02X\n\r\
		2output:\t%02X\n\r\
		3output:\t%02X\n\r\
		output:\t%02X\n\r",
		input, (unsigned char)(output), (unsigned char)(output >> 8),
		(unsigned char)(output >> 16), (unsigned char)(output >> 24));
*/
	Xil_DCacheDisable();
	Xil_ICacheDisable();
	return XST_SUCCESS;
}

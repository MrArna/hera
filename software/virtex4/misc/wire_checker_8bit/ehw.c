/*
 * @title Hera Project
 * @part Interaction with the EHW controller, fitness functions
 * @author Davide Bartolini
 * @author Fabio Cancare
 * @author Matteo Carminati
 */
#include "ehw.h" 

/* 
 * Print single columns output
 * @returns error code
 */
int printColumnsOutput(int32 input, int32 output){

	printf("[@input: %08X; 1output: %02X; 1output: %02X; 1output: %02X; output: %02X@]\n", input, (unsigned char)(output), (unsigned char)(output >> 8), (unsigned char)(output >> 16), (unsigned char)(output >> 24));

	return;
}


/*
 * Evaluate the stopping criteria
 * @returns 1 if the stopping criteria are met, 0 otherwise
 */
int resultFound(Population *pop, int fitnessMax) {
 	int i, j, k, l;
 	int found = 0;
 	if(!END_ON_MAXIMUM_FITNESS) {
 		return found;
 	}
 	else {
 		found = 1;
 		for(i = 0; i < CONVERGED_SOLUTIONS_MIN_NUMBER && found; i++) {
 			if(pop->candidates[i]->fitness < fitnessMax) {
 				found = 0;
 			}
 		}
 		if(found) {
 			printf("###@@@!!! Goal met at generation %d !!!@@@###\n", pop->generation_number);
 			if(DEBUG_LEVEL >= 2) {
				for(i = 0; i < CONVERGED_SOLUTIONS_MIN_NUMBER; i++) {
					printf("Best individual %d, fitness: %d (LUTs content):\n", i + 1, pop->candidates[i]->fitness);
					for(j = 0; j < PARTS_PER_CANDIDATE_SOLUTION; j++) {
						for(k = 0; k < MODULES_PER_PART; k++) {
							for(l = 0; l < CELLS_PER_MODULE; l++) {
								printf("0x%08X", pop->candidates[i]->parts[j].modules[k].cells[l].luts);
								if(k < MODULES_PER_PART - 1 || l < CELLS_PER_MODULE - 1) {
									printf(", ");
								}
							}
						}
					}
				}
				printf("\n\n");
 			}
 		}
 		return found;
 	}
 }

#if HWF

/*
 * Function for setting up the fitness evaluation for a comparator
 * @param fitnessMax: determines how many test cases will be generated
 */
void comparator_setup(const int lowerbound) {
	int32 a, b;
	int32 input, expected_output;
	int32 address = 0;
#if BIT32
	const int32 mostSig1 = 0x0000c000;
	const int32 lessSig1 = 0x00003000;
	const int32 mostSig2 = 0x00000c00;
	const int32 lessSig2 = 0x00000300;
	const int32 mostSig3 = 0x000000c0;
	const int32 lessSig3 = 0x00000030;
	const int32 mostSig4 = 0x0000000c;
	const int32 lessSig4 = 0x00000003;
	const int32 upperbound = ceil(sqrt(lowerbound)) + ceil(sqrt(TEST_MEMORY_SIZE));
	for(a = lowerbound; a < upperbound; a++) {
		for(b = lowerbound; b < upperbound; b++) {
			if(address >= TEST_MEMORY_SIZE) return;
			input = ((a & mostSig1) << 16) + ((b & mostSig1) << 14) + ((a & lessSig1) << 14) + ((b & lessSig1) << 12);
			input += ((a & mostSig2) << 12) + ((b & mostSig2) << 10) + ((a & lessSig2) << 10) + ((b & lessSig2) << 8);
			input += ((a & mostSig3) << 8) + ((b & mostSig3) << 6) + ((a & lessSig3) << 6) + ((b & lessSig3) << 4);
			input += ((a & mostSig4) << 4) + ((b & mostSig4) << 2) + ((a & lessSig4) << 2) + (b & lessSig4);

			expected_output = (a > b) ? ALL_ONES : ALL_ZEROS;
			EHW_CNTLR_32BIT_mWriteMemory(address*4 + XPAR_EHW_CNTLR_32BIT_0_MEM0_BASEADDR, input);
			EHW_CNTLR_32BIT_mWriteMemory(address*4 + XPAR_EHW_CNTLR_32BIT_0_MEM1_BASEADDR, expected_output);
			address++;
		}
	}
#else
	const int32 mostSig = 0x0000000c;
	const int32 lessSig = 0x00000003;
	const int32 upperbound = sqrt(fitnessMax/8);
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			input = ((a & mostSig) << 4) + ((b & mostSig) << 2) + ((a & lessSig) << 2) + (b & lessSig);
			expected_output = (a > b) ? 1 : 0;
#if XPAR_EHW_CNTLR_8BIT_NUM_INSTANCES
			EHW_CNTLR_8BIT_mWriteMemory(address*4 + XPAR_EHW_CNTLR_8BIT_0_MEM0_BASEADDR, input);
			EHW_CNTLR_8BIT_mWriteMemory(address*4 + XPAR_EHW_CNTLR_8BIT_0_MEM1_BASEADDR, expected_output);
#else
			EHW_CNTLR_32BIT_mWriteMemory(address*4 + XPAR_EHW_CNTLR_32BIT_0_MEM0_BASEADDR, input);
			EHW_CNTLR_32BIT_mWriteMemory(address*4 + XPAR_EHW_CNTLR_32BIT_0_MEM1_BASEADDR, expected_output);
#endif
			address++;
		}
	}
#endif
}

/*
 * Function for setting up the fitness evaluation for a parity generator
 */
void parity_generator_setup(const int lowerbound) {
	int32 input, expected_output, v;
#if BIT32
	for(input = lowerbound; input < (lowerbound + TEST_MEMORY_SIZE); input++) {
		//Software parity computation
		v = input;
		v ^= v >> 1;
		v ^= v >> 2;
		v = (v & 0x11111111U) * 0x11111111U;
		expected_output = (v >> 28) & 1;
		expected_output = expected_output ? ALL_ONES : ALL_ZEROS;
		EHW_CNTLR_32BIT_mWriteMemory(input*4 + XPAR_EHW_CNTLR_32BIT_0_MEM0_BASEADDR, input);
		EHW_CNTLR_32BIT_mWriteMemory(input*4 + XPAR_EHW_CNTLR_32BIT_0_MEM1_BASEADDR, expected_output);
	}
#else
	for(input = 0; input < fitnessMax; input++) {
		//Software parity computation
		v = input;
		v ^= v >> 1;
		v ^= v >> 2;
		v = (v & 0x11111111U) * 0x11111111U;
		expected_output = (v >> 28) & 1;
		expected_output = expected_output ? ALL_ONES : ALL_ZEROS;
		EHW_CNTLR_32BIT_mWriteMemory(input*4 + XPAR_EHW_CNTLR_32BIT_0_MEM0_BASEADDR, input);
		EHW_CNTLR_32BIT_mWriteMemory(input*4 + XPAR_EHW_CNTLR_32BIT_0_MEM1_BASEADDR, expected_output);
	}
#endif
}

/*
 * Function for setting up the fitness evaluation for an adder
 */
void adder_setup(const int lowerbound) {
	int32 a, b, address = 0, k;
	int32 input, expected_output;
#if BIT32
	const int32 mostSig1 = 0x0000c000;
	const int32 lessSig1 = 0x00003000;
	const int32 mostSig2 = 0x00000c00;
	const int32 lessSig2 = 0x00000300;
	const int32 mostSig3 = 0x000000c0;
	const int32 lessSig3 = 0x00000030;
	const int32 mostSig4 = 0x0000000c;
	const int32 lessSig4 = 0x00000003;
	//FIXME
	//const int32 upperbound = ceil(sqrt(TEST_MEMORY_SIZE));
	const int32 upperbound = 8;
	for(k = 0; k < 3; k++) {
		for(a = 0; a < 8; a++) {
			for(b = 0; b < 4; b++) {
				if(address >= 2048) return;
				input = ((a & mostSig1) << 16) + ((b & mostSig1) << 14) + ((a & lessSig1) << 14) + ((b & lessSig1) << 12);
				input += ((a & mostSig2) << 12) + ((b & mostSig2) << 10) + ((a & lessSig2) << 10) + ((b & lessSig2) << 8);
				input += ((a & mostSig3) << 8) + ((b & mostSig3) << 6) + ((a & lessSig3) << 6) + ((b & lessSig3) << 4);
				input += ((a & mostSig4) << 4) + ((b & mostSig4) << 2) + ((a & lessSig4) << 2) + (b & lessSig4);
				expected_output = ((a + b) << 24) + ((a + b) << 16) + ((a + b) << 8) + (a + b);
				EHW_CNTLR_32BIT_mWriteMemory(address*4 + XPAR_EHW_CNTLR_32BIT_0_MEM0_BASEADDR, input);
				EHW_CNTLR_32BIT_mWriteMemory(address*4 + XPAR_EHW_CNTLR_32BIT_0_MEM1_BASEADDR, expected_output);
				address++;
			}
		}
	}
#else
	const int32 mostSig = 0x0000000c;
	const int32 lessSig = 0x00000003;
	const int32 upperbound = sqrt(fitnessMax/8);
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			mask = 0x01000000;
			if(verbose) printf("a: %08x, b: %08x\n", a, b);
			input = ((a & mostSig) << 28) + ((b & mostSig) << 26) + ((a & lessSig) << 26) + ((b & lessSig) << 24);
			if(verbose) printf("input: %08x\n", input);
			expected_output = (a + b)*mask;
			if(verbose) printf("expected_output: %08x\n", expected_output);
			ehwWrite(input);
			output = ehwRead();
/**/		printColumnsOutput(input, output);
			if(verbose) printf("output: %08x\n", output);
			for(j = 0; j < 8; j++) {
				if((mask & output) == (mask & expected_output)) {
					fitness++;
				}
				else if(verbose) printf("FITNESS NOT INCREMENTED");
				mask = mask << 1;
			}
		}
	}
#endif
}

/*
 * Function for setting up the fitness evaluation for a multiplier
 */
void multiplier_setup(const int lowerbound) {
	int32 a, b, address = 0;
	int32 input, expected_output;
#if BIT32printColumnsOutput
	const int32 mostSig1 = 0x0000c000;
	const int32 lessSig1 = 0x00003000;
	const int32 mostSig2 = 0x00000c00;
	const int32 lessSig2 = 0x00000300;
	const int32 mostSig3 = 0x000000c0;
	const int32 lessSig3 = 0x00000030;
	const int32 mostSig4 = 0x0000000c;
	const int32 lessSig4 = 0x00000003;
	//FIXME
	//const int32 upperbound = ceil(sqrt(TEST_MEMORY_SIZE));
	const int32 upperbound = 16;
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			if(address >= 2048) return;
			input = ((a & mostSig1) << 16) + ((b & mostSig1) << 14) + ((a & lessSig1) << 14) + ((b & lessSig1) << 12);
			input += ((a & mostSig2) << 12) + ((b & mostSig2) << 10) + ((a & lessSig2) << 10) + ((b & lessSig2) << 8);
			input += ((a & mostSig3) << 8) + ((b & mostSig3) << 6) + ((a & lessSig3) << 6) + ((b & lessSig3) << 4);
			input += ((a & mostSig4) << 4) + ((b & mostSig4) << 2) + ((a & lessSig4) << 2) + (b & lessSig4);
			expected_output = ((a * b) << 16) + (a * b);
			EHW_CNTLR_32BIT_mWriteMemory(address*4 + XPAR_EHW_CNTLR_32BIT_0_MEM0_BASEADDR, input);
			EHW_CNTLR_32BIT_mWriteMemory(address*4 + XPAR_EHW_CNTLR_32BIT_0_MEM1_BASEADDR, expected_output);
			address++;
		}
	}
#else
	const int32 mostSig = 0x0000000c;
	const int32 lessSig = 0x00000003;
	const int32 upperbound = sqrt(fitnessMax/8);
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			mask = 0x01000000;
			if(verbose) printf("a: %08x, b: %08x\n", a, b);
			input = ((a & mostSig) << 28) + ((b & mostSig) << 26) + ((a & lessSig) << 26) + ((b & lessSig) << 24);
			if(verbose) printf("input: %08x\n", input);
			expected_output = (a + b)*mask;
			if(verbose) printf("expected_output: %08x\n", expected_output);
			ehwWrite(input);
			output = ehwRead();
/**/		printColumnsOutput(input, output);
			if(verbose) printf("output: %08x\n", output);
			for(j = 0; j < 8; j++) {
				if((mask & output) == (mask & expected_output)) {
					fitness++;
				}
				else if(verbose) printf("FITNESS NOT INCREMENTED");
				mask = mask << 1;
			}
		}
	}
#endif
}

void checking_memory() {
	int32 i, j;
	printf("Starting memory check\n\n");
	for(i = 0; i < TEST_MEMORY_SIZE; i++) {
		for(j = 0; j < TEST_MEMORY_SIZE; j++) {
			if(EHW_CNTLR_32BIT_mReadMemory(i*4 + XPAR_EHW_CNTLR_32BIT_0_MEM0_BASEADDR) == EHW_CNTLR_32BIT_mReadMemory(j*4 + XPAR_EHW_CNTLR_32BIT_0_MEM0_BASEADDR) && i != j) {
				printf("Error, words: %08X (%u), %08X (%u)\n", (unsigned int)EHW_CNTLR_32BIT_mReadMemory(i*4 + XPAR_EHW_CNTLR_32BIT_0_MEM0_BASEADDR), i, (unsigned int)EHW_CNTLR_32BIT_mReadMemory(j*4 + XPAR_EHW_CNTLR_32BIT_0_MEM0_BASEADDR), j);
			}
		}
	}
	printf("Ending memory check\n\n");
}

/*
 * Function for assigning a fitness value to a candidate solution in a population
 * @param int fitnessFunction: which fitness function must be used
 * @param unsigned int fitnessMax: maximum value for the fitness
 */
void fitness_setup(int fitnessFunction) {

	switch(fitnessFunction) {
		case 0:
			EHW_CNTLR_32BIT_mWriteSlaveReg0(XPAR_EHW_CNTLR_32BIT_0_BASEADDR, 0, EIGHT_BIT_FITNESS_256);
			adder_setup(0);
			//checking_memory();
		break;
		case 1:
			EHW_CNTLR_32BIT_mWriteSlaveReg0(XPAR_EHW_CNTLR_32BIT_0_BASEADDR, 0, SIXTEEN_BIT_FITNESS_256);
			multiplier_setup(0);
			//checking_memory();
		break;

		case 2:
			EHW_CNTLR_32BIT_mWriteSlaveReg0(XPAR_EHW_CNTLR_32BIT_0_BASEADDR, 0, ONE_BIT_FITNESS_4096);
			comparator_setup(0);
			checking_memory();
		break;

		case 3:
			EHW_CNTLR_32BIT_mWriteSlaveReg0(XPAR_EHW_CNTLR_32BIT_0_BASEADDR, 0, ONE_BIT_FITNESS_4096);
			parity_generator_setup(0);
			checking_memory();
		break;

		case 4:

		break;

		case 5:

		break;

		default:
			printf("The fitness function specified does not exist!\n");
			exit(1);
		break;

	}
	return;
}

void fitness(int* fitness, int fitnessFunction, unsigned int fitnessMax) {
	volatile int32 fitness_value = 0;
	int32 count = 0;
#if XPAR_EHW_CNTLR_8BIT_NUM_INSTANCES
	//Writing all zeros to the start register
	EHW_CNTLR_8BIT_mWriteSlaveReg2(XPAR_EHW_CNTLR_8BIT_0_BASEADDR, 0, ALL_ZEROS);
	//Wait for reset
	while(EHW_CNTLR_8BIT_mReadSlaveReg3(XPAR_EHW_CNTLR_8BIT_0_BASEADDR, 0) != 0x00000000);
	//Write all ones to the start register
	EHW_CNTLR_8BIT_mWriteSlaveReg2(XPAR_EHW_CNTLR_8BIT_0_BASEADDR, 0, ALL_ONES);
	//Wait for computation done
	while(EHW_CNTLR_8BIT_mReadSlaveReg3(XPAR_EHW_CNTLR_8BIT_0_BASEADDR, 0) != 0xFFFFFFFF);
	//Reading the fitness
	fitness_value = EHW_CNTLR_8BIT_mReadSlaveReg1(XPAR_EHW_CNTLR_8BIT_0_BASEADDR, 0);
#else
	do {
		if(fitnessMax > TEST_MEMORY_SIZE) {
			if(count*TEST_MEMORY_SIZE >= fitnessMax) break;
			//FIXME
			if(fitnessFunction == 2) comparator_setup(count*TEST_MEMORY_SIZE);
			if(fitnessFunction == 3) parity_generator_setup(count*TEST_MEMORY_SIZE);
		}
		count++;
		//Writing all zeros to the start register
		EHW_CNTLR_32BIT_mWriteSlaveReg2(XPAR_EHW_CNTLR_32BIT_0_BASEADDR, 0, ALL_ZEROS);
		//Wait for reset
		while(EHW_CNTLR_32BIT_mReadSlaveReg3(XPAR_EHW_CNTLR_32BIT_0_BASEADDR, 0) != 0x00000000);
		//Write all ones to the start register
		EHW_CNTLR_32BIT_mWriteSlaveReg2(XPAR_EHW_CNTLR_32BIT_0_BASEADDR, 0, ALL_ONES);
		//Wait for computation done
		while(EHW_CNTLR_32BIT_mReadSlaveReg3(XPAR_EHW_CNTLR_32BIT_0_BASEADDR, 0) != 0xFFFFFFFF);
		//Reading the fitness
		fitness_value += EHW_CNTLR_32BIT_mReadSlaveReg1(XPAR_EHW_CNTLR_32BIT_0_BASEADDR, 0);
	}
	while((fitnessFunction == 3 || fitnessFunction == 2) && fitnessMax > TEST_MEMORY_SIZE);
#endif

	*fitness = fitness_value;

	if(DEBUG_LEVEL >= 2) {
		printf("Computed fitness: %d\n", *fitness);
	}
	return;
}
#else


/*
 * Fitness function for evolve a forwarder
 * @param const unsigned int fitnesssMax: the fitness upperbound
 * @returns int fitness: the candidate solution fitness
 */
int forwarder(const unsigned int fitnessMax) {
	int32 i, j, fitness = 0;
	byte in = 0;
	int32 input, output, mask;
#if BIT32
	for(i = 0; i < fitnessMax/32; i++) {
		input = in;
		input = input << 24;
		ehwWrite(input);
		output = ehwRead();
/**/	printColumnsOutput(input, output);
		mask = 0x00000001;
		for(j = 0; j < 32; j++) {
			if((mask & output) == (mask & input)) {
				fitness++;
			}
			mask = mask << 1;
		}
		in = in + 1;
	}
#else
	for(i = 0; i < fitnessMax/8; i++) {
		input = in;
		input = input << 24;
		ehwWrite(input);
		output = ehwRead();
		mask = 0x01000000;
		for(j = 0; j < 8; j++) {
			if((mask & output) == (mask & input)) {
				fitness++;
			}
			mask = mask << 1;
		}
		in = in + 1;
	}
#endif
	return fitness;
}

/*
 * Fitness function for evolve an adder
 * @param const unsigned int fitnesssMax: the fitness upperbound
 * @returns int fitness: the candidate solution fitness
 */
int adder(const unsigned int fitnessMax, int verbose) {
	int32 a, b, j, fitness = 0;
	int32 input, output, expected_output, mask;
#if BIT32
	const int32 mask1 = 0x00010000;
	const int32 upperbound = sqrt(fitnessMax/32);
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			expected_output = a + b;
			input = a*mask1 + b;
			ehwWrite(input);
			output = ehwRead();
/**/		printColumnsOutput(input, output);
			mask = 0x00000001;
			for(j = 0; j < 32; j++){
				if((mask & output) == expected_output) {
					fitness++;
				}
				mask = mask << 1;
			}
		}
	}
#else
	const int32 mostSig = 0x0000000c;
	const int32 lessSig = 0x00000003;
	const int32 upperbound = sqrt(fitnessMax/8);
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			mask = 0x01000000;
			if(verbose) printf("a: %08x, b: %08x\n", a, b);
			input = ((a & mostSig) << 28) + ((b & mostSig) << 26) + ((a & lessSig) << 26) + ((b & lessSig) << 24);
			if(verbose) printf("input: %08x\n", input);
			expected_output = (a + b)*mask;
			if(verbose) printf("expected_output: %08x\n", expected_output);
			ehwWrite(input);
			output = ehwRead();
/**/		printColumnsOutput(input, output);
			if(verbose) printf("output: %08x\n", output);
			for(j = 0; j < 8; j++) {
				if((mask & output) == (mask & expected_output)) {
					fitness++;
				}
				else if(verbose) printf("FITNESS NOT INCREMENTED");
				mask = mask << 1;
			}
		}
	}
#endif
	return fitness;
}

/*
 * Fitness function for evolve a multiplier
 * @param const unsigned int fitnesssMax: the fitness upperbound
 * @returns int fitness: the candidate solution fitness
 */
int multiplier(const unsigned int fitnessMax) {
	int32 a, b, j, fitness = 0;
	int32 input, output, expected_output, mask;
#if BIT32
	const int32 mask1 = 0x00010000;
	const int32 upperbound = sqrt(fitnessMax/32);
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			expected_output = a * b;
			input = a*mask1 + b;
			ehwWrite(input);
			output = ehwRead();
/**/		printColumnsOutput(input, output);
			mask = 0x00000001;
			for(j = 0; j < 32; j++){
				if((mask & output) == expected_output) {
					fitness++;
				}
				mask = mask << 1;
			}
		}
	}
#else
	const int32 mask1 = 0x10000000;
	const int32 mask2 = 0x01000000;
	const int32 upperbound = sqrt(fitnessMax/8);
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			//printf("a: %08x\n", a);
			//printf("b: %08x\n", b);
			expected_output = (a * b)*mask2;
			//printf("expected_output: %08x\n", expected_output);
			input = a*mask1 + b*mask2;
			//printf("input: %08x\n", input);
			ehwWrite(input);
			output = ehwRead();
/**/		printColumnsOutput(input, output);
			//printf("output: %08x\n", output);
			mask = 0x01000000;
			for(j = 0; j < 8; j++) {
				if((mask & output) == (mask & expected_output)) {
					fitness++;
					//printf("fitness incremented\n");
				}
				mask = mask << 1;
			}
		}
	}
#endif
	return fitness;
}

/*
 * Fitness function for evolve a multiplier
 * @param const unsigned int fitnesssMax: the fitness upperbound
 * @returns int fitness: the candidate solution fitness
 */
int divider(const unsigned int fitnessMax) {
	int32 a, b, j, fitness = 0;
	int32 input, output, expected_output, mask;
#if BIT32
	const int32 mask1 = 0x00010000;
	const int32 upperbound = sqrt(fitnessMax/32);
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			expected_output = a / b;
			input = a*mask1 + b;
			ehwWrite(input);
			output = ehwRead();
/**/		printColumnsOutput(input, output);
			mask = 0x00000001;
			for(j = 0; j < 32; j++){
				if((mask & output) == expected_output) {
					fitness++;
				}
				mask = mask << 1;
			}
		}
	}
#else
	const int32 mask1 = 0x10000000;
	const int32 mask2 = 0x01000000;
	const int32 upperbound = sqrt(fitnessMax/8);
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			//printf("a: %08x\n", a);
			//printf("b: %08x\n", b);
			expected_output = (a / b)*mask2;
			//printf("expected_output: %08x\n", expected_output);
			input = a*mask1 + b*mask2;
			//printf("input: %08x\n", input);
			ehwWrite(input);
			output = ehwRead();
/**/		printColumnsOutput(input, output);
			//printf("output: %08x\n", output);
			mask = 0x01000000;
			for(j = 0; j < 8; j++) {
				if((mask & output) == (mask & expected_output)) {
					fitness++;
					//printf("fitness incremented\n");
				}
				mask = mask << 1;
			}
		}
	}
#endif
	return fitness;
}



/*
 * Fitness function for evolve an adder
 * @param const unsigned int fitnesssMax: the fitness upperbound
 * @returns int fitness: the candidate solution fitness
 */
int adder_whole(const unsigned int fitnessMax) {
	int32 a, b, j, fitness = 0;
	int32 input, output, expected_output, mask;
#if BIT32
	const int32 mask1 = 0x00010000;
	const int32 upperbound = sqrt(fitnessMax/32);
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			expected_output = a + b;
			input = a*mask1 + b;
			ehwWrite(input);
			output = ehwRead();
/**/		printColumnsOutput(input, output);
			mask = 0x00000001;
			for(j = 0; j < 32; j++){
				if((mask & output) == expected_output) {
					fitness++;
				}
				mask = mask << 1;
			}
		}
	}
#else
	const int32 mask1 = 0x10000000;
	const int32 mask2 = 0x01000000;
	mask = 0xFF000000;
	for(a = 0; a < 16; a++) {
		for(b = 0; b < 16; b++) {
			//printf("a: %08x\n", a);
			//printf("b: %08x\n", b);
			expected_output = (a + b)*mask2;
			//printf("expected_output: %08x\n", expected_output);
			input = a*mask1 + b*mask2;
			//printf("input: %08x\n", input);
			ehwWrite(input);
			output = ehwRead();
/**/		printColumnsOutput(input, output);
			//printf("output: %08x\n", output);

			if((output & mask) == (expected_output & mask)) {
				fitness++;
				//printf("fitness incremented\n");
			}
		}
	}
#endif
	return fitness;
}


/*
 * Fitness function for evolve a parity generator
 * @returns int fitness: the candidate solution fitness
 */
int parity_generator(unsigned int fitnessMax) {
	int32 j, output;
	int32 fitness = 0;
	volatile int32 v;
	int32 desired_output;
#if BIT32
	const int32 mask = 0x00000001;
	for(j = 0; j < fitnessMax; j++) {
		ehwWrite(j);
		output = ehwRead();
/**/	printColumnsOutput(input, output);
		//Software parity computation
		v = j;
		v ^= v >> 1;
		v ^= v >> 2;
		v = (v & 0x11111111U) * 0x11111111U;
		desired_output = (v >> 28) & 1;
		if((mask & output) == desired_output) {
			fitness++;
		}
	}
#else
	const int32 mask = 0x01000000;
	for(j = 0; j < fitnessMax; j++) {
		v = j*mask;
		ehwWrite(v);
		output = ehwRead();
/**/	printColumnsOutput(input, output);
		//Software parity computation
		v ^= v >> 1;
		v ^= v >> 2;
		v = (v & 0x11111111U) * 0x11111111U;
		desired_output = (v >> 28) & 1;
		desired_output = desired_output*mask;
		if((mask & output) == desired_output) {
			fitness++;
		}
	}
#endif
	return fitness;
}

/*
 * Fitness function for evolve a comparator
 * @returns int fitness: the candidate solution fitness
 */
int comparator(unsigned int fitnessMax, int verbose) {
	int32 a, b, j, fitness = 0;
	int32 input, output, expected_output, mask;
#if BIT32
	const int32 mostSig1 = 0x0000000c;
	const int32 lessSig1 = 0x00000003;
	const int32 mostSig2 = 0x000000c0;
	const int32 lessSig2 = 0x00000030;
	const int32 mostSig3 = 0x00000c00;
	const int32 lessSig3 = 0x00000300;
	const int32 mostSig4 = 0x0000c000;
	const int32 lessSig4 = 0x00003000;
	const int32 upperbound = sqrt(fitnessMax/32);
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			mask = 0x00000001;
			input = ((a & mostSig4) << 28) + ((b & mostSig4) << 26) + ((a & lessSig4) << 26) + ((b & lessSig4) << 24);
			input += ((a & mostSig3) << 20) + ((b & mostSig3) << 18) + ((a & lessSig3) << 18) + ((b & lessSig3) << 16);
			input += ((a & mostSig2) << 12) + ((b & mostSig2) << 10) + ((a & lessSig2) << 10) + ((b & lessSig2) << 8);
			input += ((a & mostSig1) << 4) + ((b & mostSig1) << 2) + ((a & lessSig1) << 2) + (b & lessSig1);
			expected_output = (a > b) ? ALL_ONES : ALL_ZEROS;
			ehwWrite(input);
			output = ehwRead();
/**/		printColumnsOutput(input, output);
			for(j = 0; j < 32; j++) {
				if((mask & output) == (mask & expected_output)) {
					fitness++;
				}
				mask = mask << 1;
			}
		}
	}
#else
	const int32 mostSig = 0x0000000c;
	const int32 lessSig = 0x00000003;
	const int32 upperbound = sqrt(fitnessMax/8);
	for(a = 0; a < upperbound; a++) {
		for(b = 0; b < upperbound; b++) {
			mask = 0x01000000;
			input = ((a & mostSig) << 28) + ((b & mostSig) << 26) + ((a & lessSig) << 26) + ((b & lessSig) << 24);
			expected_output = (a > b) ? 1 : 0;
			expected_output *= mask;
			ehwWrite(input);
			output = ehwRead();
/**/		printColumnsOutput(input, output);
			for(j = 0; j < 8; j++) {
				if((mask & output) == (mask & expected_output)) {
					fitness++;
				}
				mask = mask << 1;
			}
		}
	}
#endif
	return fitness;
}

/*
 * Write data to the candidate solution input register
 * @param int32 data: input data
 */
void ehwWrite(volatile int32 data) {
	//Write reset bit
	EHW_CORE_mWriteCtrlReg(XPAR_EHW_CORE_0_BASEADDR, 0, RESET_BIT);
	//Polling for confirmation
	while((EHW_CORE_mReadStatusReg(XPAR_EHW_CORE_0_BASEADDR, 0) & ACK_BIT_MASK) == 0);
	//Reset the control register
	EHW_CORE_mWriteCtrlReg(XPAR_EHW_CORE_0_BASEADDR, 0, 0);
	//Write the data
	EHW_CORE_mWriteInputReg(XPAR_EHW_CORE_0_BASEADDR, 0, data);
	//Start computation
	EHW_CORE_mWriteCtrlReg(XPAR_EHW_CORE_0_BASEADDR, 0, START_BIT);
}

/*
 * Read the candidate solution output register
 * @returns: int32: output data
 */
 int32 ehwRead() {
	int32 output;
 	//Wait for the end of the computation
	while((EHW_CORE_mReadStatusReg(XPAR_EHW_CORE_0_BASEADDR, 0) & ACK_BIT_MASK) == 0 );
 	//Return the content of the output register
	output = EHW_CORE_mReadOutputReg(XPAR_EHW_CORE_0_BASEADDR, 0);
 	return output;
}

/*
 * Function for assigning a fitness value to a candidate solution in a population
 * @param int *fitness: where to write the fitness value
 * @param int fitnessFunction: which fitness function must be used
 * @param unsigned int fitnessMax: maximum value for the fitness
 */
void fitness(int* fitness, int fitnessFunction, unsigned int fitnessMax, int verbose) {
	switch(fitnessFunction) {
		case 0:
			*fitness = adder(fitnessMax, verbose);
		break;

		case 1:
			*fitness = multiplier(fitnessMax);
		break;

		case 2:
			*fitness = comparator(fitnessMax, verbose);
		break;

		case 3:
			*fitness = parity_generator(fitnessMax);
		break;

		case 4:
			*fitness = forwarder(fitnessMax);
		break;

		case 5:
			* fitness = divider(fitnessMax);

		default:
			printf("The fitness function specified does not exist!\n");
			exit(1);
		break;

	}
	if(DEBUG_LEVEL >= 2) {
		printf("Computed fitness: %d\n", *fitness);
	}
	return;
}
#endif

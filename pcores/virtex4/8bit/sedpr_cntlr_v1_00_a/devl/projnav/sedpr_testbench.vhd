--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:01:02 03/29/2011
-- Design Name:   
-- Module Name:   /home/micro/hera8bit/pcores/sedpr_cntlr_v1_00_a/devl/projnav/sedpr_testbench.vhd
-- Project Name:  sedpr_cntlr
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: driver
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY sedpr_testbench IS
END sedpr_testbench;
 
ARCHITECTURE behavior OF sedpr_testbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT driver
    PORT(
         FPGA_CPU_RESET_B : IN  std_logic;
         USER_CLK : IN  std_logic;
         mem_enable : OUT  std_logic;
         DATA_IN : IN  std_logic_vector(0 to 31);
         ROM_ADDRA_32 : OUT  std_logic_vector(0 to 7);
         START : IN  std_logic_vector(0 to 31);
         PROG_DONE : OUT  std_logic_vector(0 to 31)
        );
    END COMPONENT;
    

   --Inputs
   signal FPGA_CPU_RESET_B : std_logic := '1';
   signal USER_CLK : std_logic := '0';
   signal DATA_IN : std_logic_vector(0 to 31) := (others => '0');
   signal START : std_logic_vector(0 to 31) := (others => '0');

 	--Outputs
   signal mem_enable : std_logic;
   signal ROM_ADDRA_32 : std_logic_vector(0 to 7);
   signal PROG_DONE : std_logic_vector(0 to 31);

   -- Clock period definitions
   constant USER_CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: driver PORT MAP (
          FPGA_CPU_RESET_B => FPGA_CPU_RESET_B,
          USER_CLK => USER_CLK,
          mem_enable => mem_enable,
          DATA_IN => DATA_IN,
          ROM_ADDRA_32 => ROM_ADDRA_32,
          START => START,
          PROG_DONE => PROG_DONE
        );

   -- Clock process definitions
   USER_CLK_process :process
   begin
		USER_CLK <= '0';
		wait for USER_CLK_period/2;
		USER_CLK <= '1';
		wait for USER_CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;
		
		 FPGA_CPU_RESET_B <= '0';

      wait for USER_CLK_period*10;

      START <= (others => '1');
		
		DATA_IN <= x"30018001";

      wait;
   end process;

END;

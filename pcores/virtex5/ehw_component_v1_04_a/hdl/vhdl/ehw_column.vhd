library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity ehw_column is
	generic (
		LUT_INIT: std_logic_vector(127 downto 0)
	);
	port(
		I:	in std_logic_vector(7 downto 0);
		O:	out std_logic_vector(7 downto 0);
		REC_CE: in std_logic;
		REC_CLK: in std_logic;
		REC_DATA: in std_logic_vector(3 downto 0)
	);
end ehw_column;

architecture Behavioral of ehw_column is

	signal sink : std_logic_vector(3 downto 0);

begin
	-- Cell 0 & Cell 2
	--Primitive: 5-input Dynamically Reconfigurable Look-Up Table (LUT)
	C0C2 : CFGLUT5
		generic map 
		(
			INIT => To_bitvector(LUT_INIT(127 downto 112) & LUT_INIT(95 downto 80))
		)
		port map 
		(
			CDO => sink(0), -- Reconfiguration cascade output
			O5 => O(2),   -- 4-LUT output
			O6 => O(0),   -- 5-LUT output
			CDI => REC_DATA(0), -- Reconfiguration data input
			CE => REC_CE,   -- Reconfiguration enable input
			CLK => REC_CLK, -- Clock input
			I0 => I(0),   -- Logic data input
			I1 => I(1),   -- Logic data input
			I2 => I(2),   -- Logic data input
			I3 => I(3),   -- Logic data input
			I4 => '1'    -- Logic data input, tied to 1 to use this element as two, 4-input LUTs 
		);

	-- Cell 1 & Cell 3
	C1C3 : CFGLUT5
		generic map 
		(
			INIT => To_bitvector(LUT_INIT(95 downto 80) & LUT_INIT(79 downto 64))

		)
		port map 
		(
			CDO => sink(1), -- Reconfiguration cascade output
			O5 => O(3),   -- 4-LUT output
			O6 => O(1),   -- 5-LUT output
			CDI => REC_DATA(1), -- Reconfiguration data input
			CE => REC_CE,   -- Reconfiguration enable input
			CLK => REC_CLK, -- Clock input
			I0 => I(4),   -- Logic data input
			I1 => I(5),   -- Logic data input
			I2 => I(6),   -- Logic data input
			I3 => I(7),   -- Logic data input
			I4 => '1'    -- Logic data input, tied to 1 to use this element as two, 4-input LUTs 
		);
	-- Cell 4 & Cell 6
	C4C6 : CFGLUT5
		generic map 
		(
			INIT => To_bitvector(LUT_INIT(63 downto 48) & LUT_INIT(31 downto 16))
		)
		port map 
		(
			CDO => sink(2), -- Reconfiguration cascade output
			O5 => O(6),   -- 4-LUT output
			O6 => O(4),   -- 5-LUT output
			CDI => REC_DATA(2), -- Reconfiguration data input
			CE => REC_CE,   -- Reconfiguration enable input
			CLK => REC_CLK, -- Clock input
			I0 => I(0),   -- Logic data input
			I1 => I(1),   -- Logic data input
			I2 => I(2),   -- Logic data input
			I3 => I(3),   -- Logic data input
			I4 => '1'    -- Logic data input, tied to 1 to use this element as two, 4-input LUTs 
		);
	-- Cell 5 & Cell 7
	C5C7 : CFGLUT5
		generic map 
		(
			INIT => To_bitvector(LUT_INIT(47 downto 32) & LUT_INIT(15 downto 0))
		)
		port map 
		(
			CDO => sink(3), -- Reconfiguration cascade output
			O5 => O(7),   -- 4-LUT output
			O6 => O(5),   -- 5-LUT output
			CDI => REC_DATA(3), -- Reconfiguration data input
			CE => REC_CE,   -- Reconfiguration enable input
			CLK => REC_CLK, -- Clock input
			I0 => I(4),   -- Logic data input
			I1 => I(5),   -- Logic data input
			I2 => I(6),   -- Logic data input
			I3 => I(7),   -- Logic data input
			I4 => '1'    -- Logic data input, tied to 1 to use this element as two, 4-input LUTs 
		);
end Behavioral;

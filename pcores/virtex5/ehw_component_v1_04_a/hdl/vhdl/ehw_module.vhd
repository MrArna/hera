library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ehw_module is
generic(
		LUT_INIT: std_logic_vector(511 downto 0) := X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
	);
	port(
		clock								: in std_logic;
		I									:	in std_logic_vector(7 downto 0);
		O									:	out std_logic_vector(7 downto 0);
		REC_CE							: in std_logic;
		REC_CLK							: in std_logic;
		REC_DATA							: in std_logic_vector(15 downto 0)
	);
end ehw_module;

architecture Behavioral of ehw_module is

	COMPONENT ehw_column
	GENERIC (
		LUT_INIT: std_logic_vector(127 downto 0)
	);
	PORT(
		I : IN std_logic_vector(7 downto 0);
		REC_CE : IN std_logic;
		REC_CLK : IN std_logic;
		REC_DATA : IN std_logic_vector(3 downto 0);          
		O : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;

	signal column0_input, column1_input, column2_input, column3_input : std_logic_vector(7 downto 0);	
	signal column0_output, column1_output, column2_output, column3_output : std_logic_vector(7 downto 0);

begin

	Column0: ehw_column 
	GENERIC MAP (
		LUT_INIT => LUT_INIT(511 downto 384)
	)
	PORT MAP(
		I => column0_input,
		O => column0_output,
		REC_CE => REC_CE,
		REC_CLK => REC_CLK,
		REC_DATA => REC_DATA(15 downto 12)
	);
	
	Column1: ehw_column 
	GENERIC MAP (
		LUT_INIT => LUT_INIT(383 downto 256)
	)
	PORT MAP(
		I => column1_input,
		O => column1_output,
		REC_CE => REC_CE,
		REC_CLK => REC_CLK,
		REC_DATA => REC_DATA(11 downto 8)
	);
	
	Column2: ehw_column 
	GENERIC MAP (
		LUT_INIT => LUT_INIT(255 downto 128)
	)
	PORT MAP(
		I => column2_input,
		O => column2_output,
		REC_CE => REC_CE,
		REC_CLK => REC_CLK,
		REC_DATA => REC_DATA(7 downto 4)
	);
	
	Column3: ehw_column 
	GENERIC MAP (
		LUT_INIT => LUT_INIT(127 downto 0)
	)
	PORT MAP(
		I => column3_input,
		O => column3_output,
		REC_CE => REC_CE,
		REC_CLK => REC_CLK,
		REC_DATA => REC_DATA(3 downto 0)
	);
	
	process (clock)	
	begin
		if(clock = '1' and clock'event) then
			column0_input <= I;
			column1_input <= column0_output;
			column2_input <= column1_output;
			column3_input <= column2_output;
			O <= column3_output;
		end if;
	end process;
end Behavioral;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

entity user_logic is
  generic
  (
    C_SLV_AWIDTH                   : integer              := 32;
    C_SLV_DWIDTH                   : integer              := 32;
    C_NUM_REG                      : integer              := 4;
    C_NUM_MEM                      : integer              := 1
  );
  port
  (
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Addr                    : in  std_logic_vector(0 to C_SLV_AWIDTH-1);
    Bus2IP_CS                      : in  std_logic_vector(0 to C_NUM_MEM-1);
    Bus2IP_RNW                     : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
  );

  attribute SIGIS : string;
  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of user_logic is

  	COMPONENT ehw_core_logic
	PORT(
		clock : IN std_logic;
		reset : IN std_logic;
		control_register : IN std_logic_vector(1 downto 0);
		input : IN std_logic_vector(31 downto 0);
		rec_data0 : IN std_logic_vector(31 downto 0);          
		state_register : OUT std_logic_vector(31 downto 0);
		output : OUT std_logic_vector(31 downto 0);
		rec_data_mem_address0 : OUT std_logic_vector(11 downto 0)
		);
	END COMPONENT;
	
  component DP_RAM_EHW
	port (
	   clka: in std_logic;
		ena: in std_logic;
		wea: in std_logic_vector(0 downto 0);
		addra: in std_logic_vector(11 downto 0);
		dina: in std_logic_vector(31 downto 0);
		douta: out std_logic_vector(31 downto 0);
		clkb: in std_logic;
		enb: in std_logic;
		web: in std_logic_vector(0 downto 0);
		addrb: in std_logic_vector(11 downto 0);
		dinb: in std_logic_vector(31 downto 0);
		doutb: out std_logic_vector(31 downto 0)
	   );
  end component;

  ------------------------------------------
  -- Signals for user logic slave model s/w accessible register example
  ------------------------------------------
  signal slv_reg0                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg1                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg2                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg3                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg_write_sel              : std_logic_vector(0 to 3);
  signal slv_reg_read_sel               : std_logic_vector(0 to 3);
  signal slv_ip2bus_data                : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_read_ack                   : std_logic;
  signal slv_write_ack                  : std_logic;

  ------------------------------------------
  -- Signals for user logic memory space example
  ------------------------------------------
  signal mem_data_out                  : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal mem_address							: std_logic_vector(0 to 11);
  signal mem_select							: std_logic_vector(0 to 0);
  signal mem_read_enable					: std_logic;
  signal mem_read_enable_dly1				: std_logic;
  signal mem_read_req						: std_logic;
  signal mem_ip2bus_data					: std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal mem_read_ack_dly1					: std_logic;
  signal mem_read_ack						: std_logic;
  signal mem_write_ack						: std_logic;
  signal data_in								: std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal write_enable						: std_logic_vector(0 to 0);
  signal mem_data_out_int					: std_logic_vector(31 downto 0);
  signal data_in_int							: std_logic_vector(31 downto 0);
  signal mem_address_int					: std_logic_vector(11 downto 0);
  signal core_mem_address					: std_logic_vector(11 downto 0);
  signal core_mem_data               	: std_logic_vector(31 downto 0);
  signal web									: std_logic_vector(0 to 0);
  signal dinb									: std_logic_vector(31 downto 0);
  signal dummy									: std_logic_vector(0 to 29) := (others => '0');

begin

  	Inst_ehw_core_logic: ehw_core_logic 
	PORT MAP (
		clock => Bus2IP_Clk,
		reset => Bus2IP_Reset,
		control_register => slv_reg0(30 to 31),
		state_register => slv_reg1,
		input => slv_reg2,
		output => slv_reg3,
		rec_data0 => core_mem_data,
		rec_data_mem_address0 => core_mem_address 
	);
	
	dummy <= slv_reg0(0 to 29);

  slv_reg_write_sel <= Bus2IP_WrCE(0 to 3);
  slv_reg_read_sel  <= Bus2IP_RdCE(0 to 3);
  slv_write_ack     <= Bus2IP_WrCE(0) or Bus2IP_WrCE(1) or Bus2IP_WrCE(2) or Bus2IP_WrCE(3);
  slv_read_ack      <= Bus2IP_RdCE(0) or Bus2IP_RdCE(1) or Bus2IP_RdCE(2) or Bus2IP_RdCE(3);

  -- implement slave model software accessible register(s)
  SLAVE_REG_WRITE_PROC : process( Bus2IP_Clk ) is
  begin

    if Bus2IP_Clk'event and Bus2IP_Clk = '1' then
      if Bus2IP_Reset = '1' then
        slv_reg0 <= (others => '0');
        slv_reg2 <= (others => '0');
      else
        case slv_reg_write_sel is
          when "1000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg0(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "0010" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg2(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when others => null;
        end case;
      end if;
    end if;

  end process SLAVE_REG_WRITE_PROC;

  -- implement slave model software accessible register(s) read mux
  SLAVE_REG_READ_PROC : process( slv_reg_read_sel, slv_reg1, slv_reg3 ) is
  begin

    case slv_reg_read_sel is
      when "0100" => slv_ip2bus_data <= slv_reg1;
      when "0001" => slv_ip2bus_data <= slv_reg3;
      when others => slv_ip2bus_data <= (others => '0');
    end case;

  end process SLAVE_REG_READ_PROC;

  mem_select      <= Bus2IP_CS;
  mem_read_enable <= ( Bus2IP_CS(0) ) and Bus2IP_RNW;
  mem_read_ack    <= mem_read_ack_dly1;
  mem_write_ack   <= ( Bus2IP_CS(0) ) and not(Bus2IP_RNW);
  mem_address     <= Bus2IP_Addr(C_SLV_AWIDTH-14 to C_SLV_AWIDTH-3);
  
  write_enable (0)  <= Bus2IP_CS(0) and not(Bus2IP_RNW);
  data_in <= Bus2IP_Data;

  -- implement single clock wide read request
  mem_read_req    <= mem_read_enable and not(mem_read_enable_dly1);
  BRAM_RD_REQ_PROC : process( Bus2IP_Clk ) is
  begin

    if ( Bus2IP_Clk'event and Bus2IP_Clk = '1' ) then
      if ( Bus2IP_Reset = '1' ) then
        mem_read_enable_dly1 <= '0';
      else
        mem_read_enable_dly1 <= mem_read_enable;
      end if;
    end if;

  end process BRAM_RD_REQ_PROC;

  -- this process generates the read acknowledge 1 clock after read enable
  -- is presented to the BRAM block. The BRAM block has a 1 clock delay
  -- from read enable to data out.
  BRAM_RD_ACK_PROC : process( Bus2IP_Clk ) is
  begin

    if ( Bus2IP_Clk'event and Bus2IP_Clk = '1' ) then
      if ( Bus2IP_Reset = '1' ) then
        mem_read_ack_dly1 <= '0';
      else
        mem_read_ack_dly1 <= mem_read_req;
      end if;
    end if;

  end process BRAM_RD_ACK_PROC;

  -- implement Block RAM(s)
	DP_MEM_Rec_Data : DP_RAM_EHW
		port map (
			clka => Bus2IP_Clk,
			ena => mem_select(0),
			wea => write_enable,
			addra => mem_address_int,
			dina => data_in_int,
			douta => mem_data_out_int,
			clkb => Bus2IP_Clk,
			enb => '1',
			web => web,
			addrb => core_mem_address,
			dinb => dinb,
			doutb => core_mem_data
  );
  
  ---implement data swap for memory	
data_swap_stimul: for i in 0 to 31 generate
    mem_data_out(i)  <= mem_data_out_int(31-i);
end generate;	

data_swap_address: for i in 0 to 11 generate
    mem_address_int(i)  <= mem_address(11-i);
end generate;	

data_IN_swap: for i in 0 to 31 generate
    data_in_int(i)  <= data_in(31-i);
end generate;

  -- implement Block RAM read mux
  MEM_IP2BUS_DATA_PROC : process( mem_data_out, mem_select ) is
  begin

    case mem_select is
      when "1" => mem_ip2bus_data <= mem_data_out;
      when others => mem_ip2bus_data <= (others => '0');
    end case;

  end process MEM_IP2BUS_DATA_PROC;

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= slv_ip2bus_data when slv_read_ack = '1' else
                  mem_ip2bus_data when mem_read_ack = '1' else
                  (others => '0');

  IP2Bus_WrAck <= slv_write_ack or mem_write_ack;
  IP2Bus_RdAck <= slv_read_ack or mem_read_ack;
  IP2Bus_Error <= '0';
  web <= (others =>'0');
  dinb <= (others =>'0');

end IMP;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity ehw_core_logic is
	port (
		clock								: in std_logic;
		reset								: in std_logic;
		control_register				: in std_logic_vector(1 downto 0);
		state_register					: out std_logic_vector(31 downto 0);
		input								: in std_logic_vector(31 downto 0);
		output							: out std_logic_vector(31 downto 0);
		rec_data0						: in std_logic_vector(31 downto 0);
		rec_data_mem_address0		: out std_logic_vector(11 downto 0)
	  );
end ehw_core_logic;

architecture Behavioral of ehw_core_logic is

	--32 bit implementation: 4x4 matrix of 8x4 SRLUTs
	COMPONENT ehw_part
		PORT(
			clock 									: in std_logic;
			reset										: in std_logic;
			input 									: in std_logic_vector(31 downto 0);     
			output 									: out std_logic_vector(31 downto 0);
			rec_data 								: in std_logic_vector(31 downto 0);     
			rec_data_mem_address 				: out std_logic_vector(11 downto 0);
			rec_start								: in std_logic;
			rec_done									: out std_logic
		);
	END COMPONENT;
	
	signal output_int								: std_logic_vector(31 downto 0) := (others => '0');
	signal input_int								: std_logic_vector(31 downto 0) := (others => '0');
	signal input_part0							: std_logic_vector(31 downto 0) := (others => '0');
	signal output_part0							: std_logic_vector(31 downto 0) := (others => '0');
	signal rec_start_part0						: std_logic := '0';
	signal rec_done_part0						: std_logic := '0';

	type STATE_TYPE is (WAIT_COMMAND, READ_DATA, START_RECONFIGURATION, DO_RECONFIGURATION, END_RECONFIGURATION, CLOCK_1, CLOCK_2, CLOCK_3, WRITE_DATA, WAIT_ACK);
	signal current_state : STATE_TYPE := WAIT_COMMAND;

begin

	ehw_part0: ehw_part PORT MAP(
		clock => clock,
		reset => reset,
		input => input_part0,
		output => output_part0,
		rec_data => rec_data0,
		rec_data_mem_address => rec_data_mem_address0,
		rec_start => rec_start_part0,
		rec_done => rec_done_part0
	);
	
	input_part0 <= input_int;
	output_int <= output_part0;
	
	process (clock, reset)
	variable counter: natural := 0;
	
	begin
		if reset = '1' then
			current_state <= WAIT_COMMAND;
			state_register <= (others => '1');
			counter := 0;
		
		elsif(clock = '1' and clock'event) then
			case current_state is
				when WAIT_COMMAND =>
					counter  := 0;
					state_register <= (others => '1');				
					if control_register(0) = '1' then
						current_state <= READ_DATA;
					elsif control_register(1) = '1' then
						current_state <= START_RECONFIGURATION;
					end if;	
				
				when READ_DATA =>
					input_int <= input;
					current_state <= CLOCK_1;
				
				when CLOCK_1 =>
					counter := counter + 1;
					current_state <= CLOCK_2;
				
				when CLOCK_2 =>
					current_state <= CLOCK_3;
					
				when CLOCK_3 =>					
					current_state <= WRITE_DATA;
				
				when WRITE_DATA =>
					if(counter = 4) then
						output <= output_int;
						state_register <= (0 => '1', others => '0');
						current_state <= WAIT_ACK;
					else
						current_state <= CLOCK_1;
					end if;
					
				when START_RECONFIGURATION =>
					if(rec_done_part0 = '0') then
						rec_start_part0 <= '1';
						current_state <= DO_RECONFIGURATION;
					end if;

				when DO_RECONFIGURATION =>
					rec_start_part0 <= '0';
					if(rec_done_part0 = '1') then
						current_state <= END_RECONFIGURATION;
					end if;
				
				when END_RECONFIGURATION =>
					state_register <= (1 => '1', others => '0');
					current_state <= WAIT_ACK;
					
				when WAIT_ACK =>					
					if (control_register(0) = '0' and control_register(1) = '0')  then
						current_state <= WAIT_COMMAND;
					end if;
					
				when others => 
					current_state <= WAIT_COMMAND;
			end case;
		end if;
	end process;

end Behavioral;

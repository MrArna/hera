library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity ehw_part is
	port (
		clock								: in std_logic;
		reset								: in std_logic;
		input								: in std_logic_vector(31 downto 0);
		output							: out std_logic_vector(31 downto 0);
		rec_data							: in std_logic_vector(31 downto 0);
		rec_data_mem_address			: out std_logic_vector(11 downto 0);
		rec_start						: in std_logic;
		rec_done							: out std_logic
	);
end ehw_part;

architecture Behavioral of ehw_part is

	--Base implementation: 8x4 SRLUTs, equivalent to a HERA "SingleCell"
	COMPONENT ehw_module
	PORT(
		clock : IN std_logic;
		I : IN std_logic_vector(7 downto 0);
		O : OUT std_logic_vector(7 downto 0);
		REC_CE : IN std_logic;
		REC_CLK : IN std_logic;
		REC_DATA : IN std_logic_vector(15 downto 0)	
		);
	END COMPONENT;
	
	type arr_rec_data is array(0 to 15) of std_logic_vector(15 downto 0);
	signal module_rec_data : arr_rec_data;
	
	type arr_input is array(0 to 15) of std_logic_vector(7 downto 0);
	signal module_input : arr_input;
	
	type arr_output is array(0 to 15) of std_logic_vector(7 downto 0);
	signal module_output : arr_output;

	signal module_rec_ce : std_logic_vector(15 downto 0);
	
	type STATE_TYPE is (WAIT_COMMAND, START_RECONFIGURATION, DO_RECONFIGURATION0, DO_RECONFIGURATION1, DO_RECONFIGURATION2, 
	DO_RECONFIGURATION3, DO_RECONFIGURATION4, DO_RECONFIGURATION5, DO_RECONFIGURATION6, DO_RECONFIGURATION7, END_RECONFIGURATION, WAIT_ACK);
	signal current_state : STATE_TYPE := WAIT_COMMAND;
	
	signal rec_data_mem_address_int			: std_logic_vector(11 downto 0) := (others => '0');
	signal rec_data_int							: std_logic_vector(31 downto 0) := (others => '0');

begin

  deploy_modules: FOR i IN 0 TO 15 GENERATE
	module: ehw_module 
	PORT MAP(
		clock => clock,
		I => module_input(i),
		O => module_output(i),
		REC_CE => module_rec_ce(i),
		REC_CLK => clock,
		REC_DATA => module_rec_data(i) 
	);
	END GENERATE deploy_modules;

	--modules_connection: for i in 0 to 11 generate
	--	 module_input(i+4)  <= module_output(i);
	--end generate;
	
	module_input(0) <= input(7 downto 0);
	module_input(1) <= input(15 downto 8);
	module_input(2) <= input(23 downto 16);
	module_input(3) <= input(31 downto 24);
	
	--module_input(4)(7 downto 4)  <= module_output(0)(7 downto 4);
	--module_input(4)(3 downto 0)  <= module_output(1)(7 downto 4);
	--module_input(5)(7 downto 4)  <= module_output(0)(3 downto 0);
	--module_input(5)(3 downto 0)  <= module_output(1)(3 downto 0);
	--module_input(6)  <= module_output(2);
	--module_input(7)  <= module_output(3);

	module_input(4)(7 downto 6) <= module_output(0)(7 downto 6);
	module_input(4)(5 downto 4) <= module_output(1)(7 downto 6);
	module_input(4)(3 downto 2) <= module_output(2)(7 downto 6);
	module_input(4)(1 downto 0) <= module_output(3)(7 downto 6);
	
	module_input(5)(7 downto 6) <= module_output(0)(5 downto 4);
	module_input(5)(5 downto 4) <= module_output(1)(5 downto 4);
	module_input(5)(3 downto 2) <= module_output(2)(5 downto 4);
	module_input(5)(1 downto 0) <= module_output(3)(5 downto 4);
	
	module_input(6)(7 downto 6) <= module_output(0)(3 downto 2);
	module_input(6)(5 downto 4) <= module_output(1)(3 downto 2);
	module_input(6)(3 downto 2) <= module_output(2)(3 downto 2);
	module_input(6)(1 downto 0) <= module_output(3)(3 downto 2);
	
	module_input(7)(7 downto 6) <= module_output(0)(1 downto 0);
	module_input(7)(5 downto 4) <= module_output(1)(1 downto 0);
	module_input(7)(3 downto 2) <= module_output(2)(1 downto 0);
	module_input(7)(1 downto 0) <= module_output(3)(1 downto 0);
	
	--module_input(8)  <= module_output(4);
	--module_input(9)  <= module_output(5);
	--module_input(10)  <= module_output(6);
	--module_input(11)  <= module_output(7);
	
	module_input(8)(7 downto 6) <= module_output(4)(7 downto 6);
	module_input(8)(5 downto 4) <= module_output(5)(7 downto 6);
	module_input(8)(3 downto 2) <= module_output(6)(7 downto 6);
	module_input(8)(1 downto 0) <= module_output(7)(7 downto 6);
	
	module_input(9)(7 downto 6) <= module_output(4)(5 downto 4);
	module_input(9)(5 downto 4) <= module_output(5)(5 downto 4);
	module_input(9)(3 downto 2) <= module_output(6)(5 downto 4);
	module_input(9)(1 downto 0) <= module_output(7)(5 downto 4);
	
	module_input(10)(7 downto 6) <= module_output(4)(3 downto 2);
	module_input(10)(5 downto 4) <= module_output(5)(3 downto 2);
	module_input(10)(3 downto 2) <= module_output(6)(3 downto 2);
	module_input(10)(1 downto 0) <= module_output(7)(3 downto 2);
	
	module_input(11)(7 downto 6) <= module_output(4)(1 downto 0);
	module_input(11)(5 downto 4) <= module_output(5)(1 downto 0);
	module_input(11)(3 downto 2) <= module_output(6)(1 downto 0);
	module_input(11)(1 downto 0) <= module_output(7)(1 downto 0);
	
	--module_input(12)  <= module_output(8);
	--module_input(13)  <= module_output(9);
	--module_input(14)  <= module_output(10);
	--module_input(15)  <= module_output(11);
	
	module_input(12)(7 downto 6) <= module_output(8)(7 downto 6);
	module_input(12)(5 downto 4) <= module_output(9)(7 downto 6);
	module_input(12)(3 downto 2) <= module_output(10)(7 downto 6);
	module_input(12)(1 downto 0) <= module_output(11)(7 downto 6);
	
	module_input(13)(7 downto 6) <= module_output(8)(5 downto 4);
	module_input(13)(5 downto 4) <= module_output(9)(5 downto 4);
	module_input(13)(3 downto 2) <= module_output(10)(5 downto 4);
	module_input(13)(1 downto 0) <= module_output(11)(5 downto 4);
	
	module_input(14)(7 downto 6) <= module_output(8)(3 downto 2);
	module_input(14)(5 downto 4) <= module_output(9)(3 downto 2);
	module_input(14)(3 downto 2) <= module_output(10)(3 downto 2);
	module_input(14)(1 downto 0) <= module_output(11)(3 downto 2);
	
	module_input(15)(7 downto 6) <= module_output(8)(1 downto 0);
	module_input(15)(5 downto 4) <= module_output(9)(1 downto 0);
	module_input(15)(3 downto 2) <= module_output(10)(1 downto 0);
	module_input(15)(1 downto 0) <= module_output(11)(1 downto 0);

	output <= module_output(15) & module_output(14) & module_output(13) & module_output(12);
	rec_data_mem_address <= rec_data_mem_address_int;
	rec_data_int <= rec_data;

	process (clock, reset)
	variable rec_counter: natural := 0;
	
	begin
		if reset = '1' then
			current_state <= WAIT_COMMAND;
			rec_counter := 0;
		
		elsif(clock = '1' and clock'event) then
			case current_state is
				when WAIT_COMMAND =>
					rec_counter  := 0;
					rec_done <= '0';
					if rec_start = '1' then
						current_state <= START_RECONFIGURATION;
					end if;	
					
				when START_RECONFIGURATION =>
					rec_data_mem_address_int <= (others => '0'); -- Adress of the first data word
					current_state <= DO_RECONFIGURATION0;

				when DO_RECONFIGURATION0 =>
					module_rec_ce <= (1 => '1', 0 => '1', others => '0');
					rec_data_mem_address_int <= rec_data_mem_address_int + 1;
					module_rec_data(0) <= rec_data_int(15 downto 0);
					module_rec_data(1) <= rec_data_int(31 downto 16);
					if(rec_counter = 32) then
						rec_counter := 0;
						current_state <= DO_RECONFIGURATION1;
					else
						rec_counter := rec_counter + 1;
					end if;
					
				when DO_RECONFIGURATION1 =>
					module_rec_ce <= (3 => '1', 2 => '1', others => '0');
					rec_data_mem_address_int <= rec_data_mem_address_int + 1;
					module_rec_data(2) <= rec_data_int(15 downto 0);
					module_rec_data(3) <= rec_data_int(31 downto 16);
					if(rec_counter = 32) then
						rec_counter := 0;
						current_state <= DO_RECONFIGURATION2;
					else
						rec_counter := rec_counter + 1;
					end if;
				
				when DO_RECONFIGURATION2 =>
					module_rec_ce <= (5 => '1', 4 => '1', others => '0');
					rec_data_mem_address_int <= rec_data_mem_address_int + 1;
					module_rec_data(4) <= rec_data_int(15 downto 0);
					module_rec_data(5) <= rec_data_int(31 downto 16);
					if(rec_counter = 32) then
						rec_counter := 0;
						current_state <= DO_RECONFIGURATION3;
					else
						rec_counter := rec_counter + 1;
					end if;
					
				when DO_RECONFIGURATION3 =>
					module_rec_ce <= (7 => '1', 6 => '1', others => '0');
					rec_data_mem_address_int <= rec_data_mem_address_int + 1;
					module_rec_data(6) <= rec_data_int(15 downto 0);
					module_rec_data(7) <= rec_data_int(31 downto 16);
					if(rec_counter = 32) then
						rec_counter := 0;
						current_state <= DO_RECONFIGURATION4;
					else
						rec_counter := rec_counter + 1;
					end if;
					
				when DO_RECONFIGURATION4 =>
					module_rec_ce <= (9 => '1', 8 => '1', others => '0');
					rec_data_mem_address_int <= rec_data_mem_address_int + 1;
					module_rec_data(8) <= rec_data_int(15 downto 0);
					module_rec_data(9) <= rec_data_int(31 downto 16);
					if(rec_counter = 32) then
						rec_counter := 0;
						current_state <= DO_RECONFIGURATION5;
					else
						rec_counter := rec_counter + 1;
					end if;
					
				when DO_RECONFIGURATION5 =>
					module_rec_ce <= (11 => '1', 10 => '1', others => '0');
					rec_data_mem_address_int <= rec_data_mem_address_int + 1;
					module_rec_data(10) <= rec_data_int(15 downto 0);
					module_rec_data(11) <= rec_data_int(31 downto 16);
					if(rec_counter = 32) then
						rec_counter := 0;
						current_state <= DO_RECONFIGURATION6;
					else
						rec_counter := rec_counter + 1;
					end if;
					
				when DO_RECONFIGURATION6 =>
					module_rec_ce <= (13 => '1', 12 => '1', others => '0');
					rec_data_mem_address_int <= rec_data_mem_address_int + 1;
					module_rec_data(12) <= rec_data_int(15 downto 0);
					module_rec_data(13) <= rec_data_int(31 downto 16);
					if(rec_counter = 32) then
						rec_counter := 0;
						current_state <= DO_RECONFIGURATION7;
					else
						rec_counter := rec_counter + 1;
					end if;
					
				when DO_RECONFIGURATION7 =>
					module_rec_ce <= (15 => '1', 14 => '1', others => '0');
					rec_data_mem_address_int <= rec_data_mem_address_int + 1;
					module_rec_data(14) <= rec_data_int(15 downto 0);
					module_rec_data(15) <= rec_data_int(31 downto 16);
					if(rec_counter = 32) then
						rec_counter := 0;
						current_state <= END_RECONFIGURATION;
					else
						rec_counter := rec_counter + 1;
					end if;
				
				when END_RECONFIGURATION =>
					rec_done <= '1';
					current_state <= WAIT_ACK;
					
				when WAIT_ACK =>				
					if (rec_start = '0') then
						current_state <= WAIT_COMMAND;
					end if;
					
				when others => 
					current_state <= WAIT_COMMAND;
			end case;
		end if;
	end process;

end Behavioral;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library work;
use work.types_pkg.all;

entity individual is
	port (
		i : in std_logic_vector(31 downto 0);
		o : out std_logic_vector(31 downto 0);
		eval : in std_logic;
		out_col : in integer range 0 to 15;
		io_ready : out std_logic;
		clk : in std_logic;
		rec_data : in std_logic_vector(15 downto 0);
		col_addr : in integer range 0 to 15;
		cell_addr : in integer range 0 to 31;
		reconf : in std_logic;
		reconf_ack : out std_logic;
		conf_valid : out std_logic
	);
end individual;

architecture Behavioral of individual is

	component column is
		generic (
			routing : colrout_arr
		);
		port (
			i : in std_logic_vector(31 downto 0);
			o : out std_logic_vector(31 downto 0);
			clk : in std_logic;
			rec_data : in std_logic_vector(15 downto 0);
			rec_addr : in integer range 0 to 31;
			reconf : in std_logic;
			ack : out std_logic;
			valid : out std_logic
		);
	end component;

	constant crout_i : colrout_arr := (
						3, 2, 1, 0, --for cells 0,2
						7, 6, 5, 4, --for cells 1,3
						3, 2, 1, 0, --for cells 4,6
						7, 6, 5, 4, --for cells 5,7

						11, 10, 9, 8, -- ...
						15, 14, 13, 12,
						11, 10, 9, 8,
						15, 14, 13, 12,

						19, 18, 17, 16,
						23, 22, 21, 20, 
						19, 18, 17, 16,
						23, 22, 21, 20, 

						27, 26, 25, 24,
						31, 30, 29, 28,
						27, 26, 25, 24,
						31, 30, 29, 28
					);
	constant crout_m : colrout_arr := (
						23, 22, 31, 30,
						7, 6, 15, 14,
						23, 22, 31, 30,
						7, 6, 15, 14,

						21, 20, 29, 28,
						5, 4, 13, 12,
						21, 20, 29, 28,
						5, 4, 13, 12,

						19, 18, 27, 26,
						3, 2, 11, 10, 
						19, 18, 27, 26,
						3, 2, 11, 10, 

						17, 16, 25, 24,
						1, 0, 9, 8,
						17, 16, 25, 24,
						1, 0, 9, 8
					);
	type col_io_arr is array(0 to 15) of std_logic_vector(31 downto 0);
	signal col_in : col_io_arr;
	signal col_out : col_io_arr;
	type cols_rout_arr is array(0 to 15) of colrout_arr;
	constant cols_rout : cols_rout_arr := (
						crout_i, crout_i, crout_i, crout_i,
						crout_m, crout_i, crout_i, crout_i,
						crout_m, crout_i, crout_i, crout_i,
						crout_m, crout_i, crout_i, crout_i
					);

	type rec_state_t is (GET_ADDR, GIVE_ACK, DONE);
	signal rec_state_cur, rec_state_next : rec_state_t := GET_ADDR;
	type rec_data_arr is array(0 to 15) of std_logic_vector(15 downto 0);
	signal rec_data_cur, rec_data_next : rec_data_arr;
	type cell_addr_arr is array(0 to 15) of integer range 0 to 31;
	signal cell_addr_cur, cell_addr_next : cell_addr_arr;
	signal col_reconf_cur, col_reconf_next : std_logic_vector(15 downto 0) :=
									(others => '0');
	signal col_ack, col_valid : std_logic_vector(15 downto 0) := (others => '0');

	signal col_addr_cur, col_addr_next : integer range 0 to 15;
	signal confvalid_cur, confvalid_next : std_logic := '0';
	signal reconf_ack_buf, recack_cur, recack_next : std_logic := '0';
	constant rec_data_init : std_logic_vector(15 downto 0) := x"FFFE";

	type io_state_t is (GET_I, EVL);
	signal io_state_cur, io_state_next : io_state_t := GET_I;
	signal io_ready_cur, io_ready_next : std_logic := '0';
	signal io_cnt_cur, io_cnt_next : integer range 0 to 3 := 0;
	type col_io_buf_t is array(0 to 2) of std_logic_vector(31 downto 0);
	signal col_i_cur, col_i_next : col_io_buf_t;
	signal outcol_cur, outcol_next : integer range 0 to 15 := 15;
	signal i_cur, i_next : std_logic_vector(31 downto 0);
	signal o_cur, o_next : std_logic_vector(31 downto 0);
	
begin
	gen_cols: for n in 0 to 15 generate
		col : column
		generic map (
			routing => cols_rout(n)
		)
		port map (
			i => col_in(n),
			o => col_out(n),
			clk => clk,
			rec_data => rec_data_cur(n),
			rec_addr => cell_addr_cur(n),
			reconf => col_reconf_cur(n),
			ack => col_ack(n),
			valid => col_valid(n)
		);
	end generate;

	reconf_comb: process (reconf, rec_state_cur, rec_data, reconf_ack_buf, recack_cur, col_valid,
		col_addr, cell_addr, col_reconf_cur, cell_addr_cur, col_addr_cur, rec_data_cur, confvalid_cur)
	begin
		rec_state_next <= rec_state_cur;
		col_addr_next <= col_addr_cur;
		cell_addr_next <= cell_addr_cur;
		col_reconf_next <= col_reconf_cur;
		rec_data_next <= rec_data_cur;
		recack_next <= reconf_ack_buf;
		confvalid_next <= confvalid_cur;

		case rec_state_cur is
			when GET_ADDR =>
				col_addr_next <= col_addr;
				cell_addr_next(col_addr) <= cell_addr;
				confvalid_next <= '1';
				if reconf = '1' then
					confvalid_next <= '0';
					rec_data_next(col_addr) <= rec_data;
					col_reconf_next(col_addr) <= '1';
					rec_state_next <= GIVE_ACK;
				end if;
			when GIVE_ACK =>
				if recack_cur = '1' then
					col_reconf_next(col_addr_cur) <= reconf;
					rec_state_next <= DONE;
				end if;
			when DONE =>
				col_reconf_next(col_addr_cur) <= reconf;
				if recack_cur = '0' and reconf = '0' and col_valid = x"FFFF" then
					confvalid_next <= '1';
					rec_state_next <= GET_ADDR;
				end if;
		end case;
	end process;

	reconf_sync: process (clk)
	begin
		if rising_edge(clk) then
			rec_data_cur <= rec_data_next;
			col_addr_cur <= col_addr_next;
			cell_addr_cur <= cell_addr_next;
			rec_state_cur <= rec_state_next;
			col_reconf_cur <= col_reconf_next;
			confvalid_cur <= confvalid_next;
			recack_cur <= recack_next;
		end if;
	end process;

	io_comb: process(i, io_ready_cur, io_state_cur, io_cnt_cur, i_cur, i, o_cur,
					outcol_cur, out_col, eval, col_out, col_i_cur)
	begin
		io_state_next <= io_state_cur;
		io_ready_next <= io_ready_cur;
		io_cnt_next <= io_cnt_cur;
		col_i_next <= col_i_cur;
		outcol_next <= outcol_cur;
		i_next <= i_cur;
		o_next <= o_cur;

		case io_state_cur is
			when GET_I =>
				if eval = '1' then
					i_next <= i;
					io_cnt_next <= 0;
					io_ready_next <= '0';
					outcol_next <= out_col;
					io_state_next <= EVL;
				end if;
			when EVL =>
				if io_cnt_cur < (outcol_cur / 4) then
					io_cnt_next <= io_cnt_cur + 1;
					for n in 0 to 2 loop
						col_i_next(n) <= col_out(n * 4 + 3);
					end loop;
				else
					o_next <= col_out(outcol_cur);
					io_ready_next <= '1';
				end if;
				if eval = '0' then
					io_state_next <= GET_I;
				end if;
		end case;
	end process;

	io_sync: process(clk)
	begin
		if rising_edge(clk) then
			col_i_cur <= col_i_next;
			io_ready_cur <= io_ready_next;
			io_state_cur <= io_state_next;
			io_cnt_cur <= io_cnt_next;
			outcol_cur <= outcol_next;
			i_cur <= i_next;
			o_cur <= o_next;
		end if;
	end process;

	colscon_comb: for n in 0 to 3 generate
		colsseq: for m in (n * 4 + 1) to (n * 4 + 3) generate
			col_in(m) <= col_out(m - 1);
		end generate;
	end generate;
	colscon_seq: for n in 1 to 3 generate
		col_in(n * 4) <= col_i_cur(n - 1);
	end generate;
	col_in(0) <= i_cur;
	o <= o_cur;
	reconf_ack_buf <= col_ack(col_addr_cur);
	reconf_ack <= recack_cur;
	io_ready <= io_ready_cur;
	conf_valid <= confvalid_cur;

end Behavioral;


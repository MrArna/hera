#!/bin/bash

SCRF=".syntax_check.scr"
PRJF=".syntax_check.prj"

echo "elaborate" > ${SCRF}
echo "-ifn ${PRJF}" >> ${SCRF}

echo "# Files to check" > ${PRJF}

if [ "$*" == "" ]; then
	echo "Checking *.vhdl .."
	for f in *.vhd; do
		echo "vhdl work \"${f}\"" >> ${PRJF}
	done
else
	echo "Checking $* .."
	for f in $*; do
		echo "vhdl work \"${f}\"" >> ${PRJF}
	done
fi

xst -ifn ${SCRF}

rm -r xst
scrname=${0##*/}
rm -f .${scrname%.sh}*
rm -f .lso


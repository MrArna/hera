package types_pkg is
	type colrout_arr is array(0 to 63) of integer range 0 to 31;
end types_pkg;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.types_pkg.all;

entity column is
	generic (
		routing : colrout_arr := (
						3, 2, 1, 0, --for cells 0,2
						7, 6, 5, 4, --for cells 1,3
						3, 2, 1, 0, --for cells 4,6
						7, 6, 5, 4, --for cells 5,7

						11, 10, 9, 8, -- ...
						15, 14, 13, 12,
						11, 10, 9, 8,
						15, 14, 13, 12,

						19, 18, 17, 16,
						23, 22, 21, 20, 
						19, 18, 17, 16,
						23, 22, 21, 20, 

						27, 26, 25, 24,
						31, 30, 29, 28,
						27, 26, 25, 24,
						31, 30, 29, 28
					)
	);
	port (
		i : in std_logic_vector(31 downto 0);
		o : out std_logic_vector(31 downto 0);
		clk : in std_logic;
		rec_data : in std_logic_vector(15 downto 0);
		rec_addr : in integer range 0 to 31;
		reconf : in std_logic;
		ack : out std_logic;
		valid : out std_logic
	);
end column;

architecture Behavioral of column is

component twincfglut4 is
	generic (
		init_cfg0 : std_logic_vector(15 downto 0);
		init_cfg1 : std_logic_vector(15 downto 0)
	);
	port (
		i0 : in std_logic;
		i1 : in std_logic;
		i2 : in std_logic;
		i3 : in std_logic;
		cfg0 : in std_logic_vector(15 downto 0);
		cfg1 : in std_logic_vector(15 downto 0);
		clk : in std_logic;
		o0 : out std_logic;
		o1 : out std_logic;
		reconf0 : in std_logic;
		reconf1 : in std_logic;
		ack : out std_logic;
		valid : out std_logic
	);
end component;

	signal lutcfg_cur, lutcfg_next : std_logic_vector(511 downto 0);
	signal cell_reconf_cur, cell_reconf_next : std_logic_vector(31 downto 0) := (others => '0');
	signal cell_ack : std_logic_vector(15 downto 0) := (others => '0');
	signal cell_valid : std_logic_vector(15 downto 0) := (others => '0');
	type STATE_TYPE is (GET_ADDR, GIVE_ACK, DONE);
	signal state_cur, state_next : STATE_TYPE := GET_ADDR;
	signal ack_buf, ack_cur, ack_next : std_logic := '0';
	signal rec_addr_cur, rec_addr_next : integer range 0 to 31;
	constant lutcfg_init : std_logic_vector(15 downto 0) := x"FFFE";
	signal valid_cur, valid_next : std_logic := '0';

begin
	gen_cells: for n in 0 to 7 generate
		even_cell : twincfglut4
		generic map (
			init_cfg0 => lutcfg_init,
			init_cfg1 => lutcfg_init
		)
		port map (
			i0 => i(routing(n * 8 + 0)),
			i1 => i(routing(n * 8 + 1)),
			i2 => i(routing(n * 8 + 2)),
			i3 => i(routing(n * 8 + 3)),
			cfg0 => lutcfg_cur(((n * 4 + 0) * 16 + 15) downto ((n * 4 + 0) * 16)),
			cfg1 => lutcfg_cur(((n * 4 + 2) * 16 + 15) downto ((n * 4 + 2) * 16)),
			clk => clk,
			o0 => o(n * 4),
			o1 => o(n * 4 + 2),
			reconf0 => cell_reconf_cur(n * 4 + 0),
			reconf1 => cell_reconf_cur(n * 4 + 2),
			ack => cell_ack(n * 2),
			valid => cell_valid(n * 2)
		);
		odd_cell : twincfglut4
		generic map (
			init_cfg0 => lutcfg_init,
			init_cfg1 => lutcfg_init
		)
		port map (
			i0 => i(routing(n * 8 + 4)),
			i1 => i(routing(n * 8 + 5)),
			i2 => i(routing(n * 8 + 6)),
			i3 => i(routing(n * 8 + 7)),
			cfg0 => lutcfg_cur(((n * 4 + 1) * 16 + 15)
						downto ((n * 4 + 1) * 16)),
			cfg1 => lutcfg_cur(((n * 4 + 3) * 16 + 15)
						downto ((n * 4 + 3) * 16)),
			clk => clk,
			o0 => o(n * 4 + 1),
			o1 => o(n * 4 + 3),
			reconf0 => cell_reconf_cur(n * 4 + 1),
			reconf1 => cell_reconf_cur(n * 4 + 3),
			ack => cell_ack(n * 2 + 1),
			valid => cell_valid(n * 2 + 1)
		);
	end generate;

	reconf_comb: process (reconf, state_cur, rec_data, ack_buf, ack_cur, rec_addr, valid_cur,
					cell_valid, cell_reconf_cur, rec_addr_cur, lutcfg_cur)
	begin
		state_next <= state_cur;
		rec_addr_next <= rec_addr_cur;
		lutcfg_next <= lutcfg_cur;
		cell_reconf_next <= cell_reconf_cur;
		ack_next <= ack_buf;
		valid_next <= valid_cur;

		case state_cur is
			when GET_ADDR =>
				rec_addr_next <= rec_addr;
				valid_next <= '1';
				if reconf = '1' then
					valid_next <= '0';
					lutcfg_next((rec_addr * 16 + 15)
						downto (rec_addr * 16)) <= rec_data;
					cell_reconf_next(rec_addr) <= '1';
					state_next <= GIVE_ACK;
				end if;
			when GIVE_ACK =>
				if ack_cur = '1' then
					cell_reconf_next(rec_addr_cur) <= reconf;
					state_next <= DONE;
				end if;
			when DONE =>
				cell_reconf_next(rec_addr_cur) <= reconf;
				if ack_cur = '0' and reconf = '0' and cell_valid = x"FFFF" then
					valid_next <= '1';
					state_next <= GET_ADDR;
				end if;
		end case;
	end process;

	reconf_sync: process (clk)
	begin
		if rising_edge(clk) then
			lutcfg_cur <= lutcfg_next;
			rec_addr_cur <= rec_addr_next;
			state_cur <= state_next;
			cell_reconf_cur <= cell_reconf_next;
			valid_cur <= valid_next;
			ack_cur <= ack_next;
		end if;
	end process;

	ack_buf <= cell_ack(2 * (rec_addr_cur / 4) + rec_addr_cur mod 2);
	ack <= ack_cur;
	valid <= valid_cur;

end Behavioral;


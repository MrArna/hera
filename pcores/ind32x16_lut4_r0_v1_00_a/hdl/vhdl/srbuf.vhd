library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity srbuf is
	port (
		i : in std_logic_vector(31 downto 0);
		q : out std_logic;
		clk : in std_logic;
		shift : in std_logic;
		ack : out std_logic;
		valid : out std_logic;
		cnt : out std_logic_vector(4 downto 0)
	);
end srbuf;

architecture Behavioral of srbuf is

	type STATE_TYPE is (STABLE, SHFT, DONE);
	signal state_cur, state_next : STATE_TYPE := STABLE;

	signal data_cur, data_next : std_logic_vector(31 downto 0) := (others => '0');
	signal valid_cur, valid_next : std_logic := '0';
	signal cnt_cur, cnt_next : integer range 0 to 31 := 0;
	signal ack_cur, ack_next : std_logic := '0';
begin
	fsa_comb: process (state_cur, i, shift, cnt_cur, valid_cur, data_cur, ack_cur)
	begin
		state_next <= state_cur;
		cnt_next <= cnt_cur;
		valid_next <= valid_cur;
		ack_next <= ack_cur;
		data_next <= data_cur;

		case state_cur is
			when STABLE =>
				data_next <= i;
				valid_next <= '0';
				if shift = '1' then
					cnt_next <= 31;
					valid_next <= '1';
					ack_next <= '1';
					state_next <= SHFT;
				end if;
			when SHFT =>
				if cnt_cur > 0 then
					cnt_next <= cnt_cur - 1;
				end if;
				if cnt_cur = 0 then
					valid_next <= '0';
					state_next <= DONE;
				end if;
				if shift = '0' then
					ack_next<= '0';
				end if;
			when DONE =>
				if shift = '0' then
					if ack_cur /= '0' then
						ack_next <= '0';
					end if;
					state_next <= STABLE;
				end if;
		end case;
	end process;

	fsa_synch: process (clk)
	begin
		if rising_edge(clk) then
			state_cur <= state_next;
			cnt_cur <= cnt_next;
			valid_cur <= valid_next;
			ack_cur <= ack_next;
			data_cur <= data_next;
		end if;
	end process;

	q <= data_cur(cnt_cur);
	valid <= valid_cur;
	ack <= ack_cur;
	cnt <= std_logic_vector(to_unsigned(cnt_cur, cnt'length));

end Behavioral;


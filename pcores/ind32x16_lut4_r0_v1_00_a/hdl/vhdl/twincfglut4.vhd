library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity twincfglut4 is
	generic (
		init_cfg0 : std_logic_vector(15 downto 0) := x"0000";
		init_cfg1 : std_logic_vector(15 downto 0) := x"0000"
	);
	port (
		i0 : in std_logic;
		i1 : in std_logic;
		i2 : in std_logic;
		i3 : in std_logic;
		cfg0 : in std_logic_vector(15 downto 0);
		cfg1 : in std_logic_vector(15 downto 0);
		clk : in std_logic;
		o0 : out std_logic;
		o1 : out std_logic;
		reconf0 : in std_logic;
		reconf1 : in std_logic;
		ack : out std_logic;
		valid : out std_logic
	);
end twincfglut4;

architecture Behavioral of twincfglut4 is
component srbuf is
	port (
		i : in std_logic_vector(31 downto 0);
		q : out std_logic;
		clk : in std_logic;
		shift : in std_logic;
		ack : out std_logic;
		valid : out std_logic;
		cnt : out std_logic_vector(4 downto 0)
	);
end component;

signal recdata : std_logic := '0';
signal recvalid : std_logic := '0';
signal valid_cur, valid_next : std_logic := '0';
signal reconf_cur, reconf_next : std_logic := '0';
signal cfg0_cur, cfg0_next : std_logic_vector(15 downto 0) := init_cfg0;
signal cfg1_cur, cfg1_next : std_logic_vector(15 downto 0) := init_cfg1;
signal sink : std_logic_vector(5 downto 0) := (others => '0');

begin

	buf : srbuf 
	port map(
		i(31 downto 16) => cfg1_cur,
		i(15 downto 0) => cfg0_cur,
		q => recdata,
		clk => clk,
		shift => reconf_cur,
		ack => ack,
		valid => recvalid,
		cnt => sink(5 downto 1)
	);

	luts : CFGLUT5
	generic map (
		INIT => to_bitvector(init_cfg0 & init_cfg1)
	)
	port map (
		CDO => sink(0),
		O5 => o0,
		O6 => o1,
		CDI => recdata,
		CE => recvalid,
		CLK => clk,
		I0 => i0,
		I1 => i1,
		I2 => i2,
		I3 => i3,
		I4 => '1'
	);

	updatecfg: process (cfg0, cfg1, cfg0_cur, cfg1_cur,
				reconf0, reconf1, recvalid, valid_cur)
	begin
		cfg0_next <= cfg0_cur;
		cfg1_next <= cfg1_cur;
		reconf_next <= (reconf0 or reconf1);
		valid_next <= not recvalid;

		if reconf0 = '1' then
			if (cfg0 /= cfg0_cur) then
				cfg0_next <= cfg0;
			end if;
		end if;
		if reconf1 = '1' then
			if (cfg1 /= cfg1_cur) then
				cfg1_next <= cfg1;
			end if;
		end if;
	end process;

	updatecfg_sync: process (clk)
	begin
		if rising_edge(clk) then
			cfg0_cur <= cfg0_next;
			cfg1_cur <= cfg1_next;
			reconf_cur <= reconf_next;
			valid_cur <= valid_next;
		end if;
	end process;

	valid <= valid_cur;

end Behavioral;

